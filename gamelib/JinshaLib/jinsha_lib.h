#pragma once
#include "vector.h"

#include <D3D11.h>

namespace JinshaLib
{
	bool Initialize(const char* window_name, const int width, const int height, bool fullscreen, HINSTANCE instance);
	void UnInitialize();

	bool GameLoop();
	void ClearScreen();
	void ScreenPresent(unsigned int sync_interval, unsigned int flags);
	void SetScreenColor(const float r, const float g, const float b);

	float GetElapsedTime();
	void ResetHighResolutionTimer();

	void RenderingBegin();
	void RenderingEnd();
	float GetAverageRenderingTime();
	
	ID3D11Device*		 GetDevice();
	ID3D11DeviceContext* GetContext();
	HWND				 GetHWND();
	HINSTANCE			 GetHINSTANCE();
}
