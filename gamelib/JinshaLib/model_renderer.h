#pragma once


#include "model.h"
#include "constant_buffer.h"
#include "shader.h"


#include <memory>


class ModelRenderer
{
public:
	ModelRenderer(ID3D11Device* device);
	~ModelRenderer() {}

	void Begin(ID3D11DeviceContext* immediate_context, const FLOAT4X4& view_projection, const VECTOR4F& light_direction);
	void Render(ID3D11DeviceContext* immediate_context, const Model& model, const VECTOR4F& color);
	void End(ID3D11DeviceContext* immediate_context);

private:
	static const int MAX_BONES = 100;

	struct CbScene
	{
		FLOAT4X4	view_projection;
		VECTOR4F	light_direction;
	};

	struct CbMesh
	{
		//DirectX::XMFLOAT4X4 world;
		FLOAT4X4	bone_transforms[MAX_BONES];
	};

	struct CbSubset
	{
		VECTOR4F	material_color;
	};


	std::unique_ptr<ConstantBuffer<CbScene>>	m_p_cb_scene;
	std::unique_ptr<ConstantBuffer<CbMesh>>		m_p_cb_mesh;
	std::unique_ptr<ConstantBuffer<CbSubset>>	m_p_cb_subset;

	std::unique_ptr<VertexShader>				m_p_vertex_shader;
	std::unique_ptr<PixelShader>				m_p_pixel_shader;
};
