#include "static_mesh.h"

StaticMesh::StaticMesh(ID3D11Device* device, const char* filename, bool flipping_v_coodinates)
{
	std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>(filename);
	std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
	model = std::make_unique<Model>(model_resource);

	model_renderer = std::make_unique<ModelRenderer>(device);
}

void StaticMesh::Render(ID3D11DeviceContext* device_context,
	const FLOAT4X4& view_projection,
	const VECTOR4F& light,
	const VECTOR4F& color)
{

}