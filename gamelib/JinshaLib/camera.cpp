#include "camera.h"

Camera::Camera(const VECTOR3F& eye, const VECTOR3F& focus, const VECTOR3F& up, bool perspective_projection, float fovy_or_view_width, float aspect_ratio, float near_z, float far_z)
	: m_eye(eye), m_focus(focus), m_up(up), m_perspective_projection(perspective_projection), m_fovy_or_view_width(fovy_or_view_width), m_aspect_ratio(aspect_ratio), m_near_z(near_z), m_far_z(far_z)
{
	float x = m_focus.x - m_eye.x;
	float y = m_focus.y - m_eye.y;
	float z = m_focus.z - m_eye.z;

	m_distance = ::sqrtf(x * x + y * y + z * z);
}

void Camera::Update()
{
	using namespace DirectX;
	XMVECTOR E = XMVectorSet(m_eye.x, m_eye.y, -m_eye.z, 1.0f);
	XMVECTOR F = XMVectorSet(m_focus.x, m_focus.y, -m_focus.z, 1.0f);
	XMVECTOR U = XMVectorSet(m_up.x, m_up.y, m_up.z, 1.0f);

	XMMATRIX V = XMMatrixLookAtLH(E, F, U);
	XMStoreFloat4x4(&m_view, V);

	XMMATRIX P = m_perspective_projection ? XMMatrixPerspectiveFovLH(m_fovy_or_view_width * 0.01745f, m_aspect_ratio, m_near_z, m_far_z) : XMMatrixOrthographicLH(m_fovy_or_view_width, m_fovy_or_view_width / m_aspect_ratio, 1.0f, 200.0f);
	XMStoreFloat4x4(&m_projection, P);
}

void Camera::Reset(const VECTOR3F& eye, const VECTOR3F& focus, const VECTOR3F& up)
{
	m_eye = eye; m_focus = focus; m_up = up;
}