//**************************************
//   include Headers
//**************************************
#include "texture.h"
#include "logger.h"

#include <wrl.h>
#include <shlwapi.h>
#include <map>
#include <sstream>

//**************************************
//   Library methods
//**************************************
namespace JinshaLib
{
	static std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>	g_textures;
	void LoadTextureFromFile(ID3D11Device* device, const char* filename,
		ID3D11ShaderResourceView** srv, bool force_srgb)
	{
		wchar_t filename_w[256];
		mbstowcs_s(0, filename_w, filename, strlen(filename) + 1);

		HRESULT hr = S_OK;

		std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator it = g_textures.find(filename_w);
		if (it != g_textures.end())
		{
			*srv = it->second.Get();
			(*srv)->AddRef();
			return;
		}
		else
		{
			DirectX::TexMetadata  metadata;
			DirectX::ScratchImage image;

			std::wstring extension = PathFindExtensionW(filename_w);
			//std::transform(extension.begin(), extension.end(), extension.begin(), ::towlower);

			if (extension == L".png" || extension == L".jpeg" || extension == L".jpg" || extension == L".JPG")
			{
				hr = DirectX::LoadFromWICFile(filename_w, 0, &metadata, image);
			}
			else if (extension == L".tga")
			{
				hr = DirectX::LoadFromTGAFile(filename_w, &metadata, image);
			}
			else
			{
				LOGGER("*********************************\n");
				LOGGER("LoadTextureFromFile() extension\n");
				LOGGER("*********************************\n");
				assert(!"File format not found");
			}
			if (FAILED(hr))
			{
				LOGGER("**********************************\n");
				LOGGER("LoadTextureFromFile() LoadFromFile\n");
				LOGGER("**********************************\n");
				assert(!"Unable to load texture from file");
			}

			hr = DirectX::CreateShaderResourceViewEx(
				device,								 //	ID3D11Device* pDevice,
				image.GetImages(),					 //	const Image* srcImages,
				image.GetImageCount(),				 //	size_t nimages,
				metadata,							 //	const TexMetadata& metadata,
				D3D11_USAGE_DEFAULT,				 //	D3D11_USAGE usage,
				D3D11_BIND_SHADER_RESOURCE,			 //	unsigned int bindFlags,
				0,									 //	unsigned int cpuAccessFlags,
				D3D11_RESOURCE_MISC_TEXTURECUBE,	 //	unsigned int miscFlags,
				force_srgb,							 //	bool forceSRGB,
				srv);								 //	ID3D11ShaderResourceView** ppSRV);

			if (FAILED(hr))
			{
				LOGGER("**********************************\n");
				LOGGER("CreateShaderResourceViewEx()\n");
				LOGGER("**********************************\n");
				assert(!"Could not Create a ShaderResourceView from a texture");
			}

			g_textures.insert(std::make_pair(filename_w, *srv));
		}
	}
	void MakeDummyTexture(ID3D11Device* device, ID3D11ShaderResourceView** srv, unsigned int value, bool force_srgb)
	{
		std::stringstream filename;
		filename << "dummy_texture" << "." << std::hex << std::uppercase << value << "." << force_srgb;

		wchar_t filename_w[256];

		std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator it = g_textures.find(filename_w);
		if (it != g_textures.end())
		{
			*srv = it->second.Get();
			(*srv)->AddRef();
		}
		else
		{
			HRESULT hr = S_OK;

			D3D11_TEXTURE2D_DESC texture2d_desc = {};
			texture2d_desc.Width = 1;
			texture2d_desc.Height = 1;
			texture2d_desc.MipLevels = 1;
			texture2d_desc.ArraySize = 1;
			texture2d_desc.Format = force_srgb ? DXGI_FORMAT_R8G8B8A8_UNORM_SRGB : DXGI_FORMAT_R8G8B8A8_UNORM;
			texture2d_desc.SampleDesc.Count = 1;
			texture2d_desc.SampleDesc.Quality = 0;
			texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
			texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			texture2d_desc.CPUAccessFlags = 0;
			texture2d_desc.MiscFlags = 0;

			unsigned char material_color[] = { UCHAR_MAX, UCHAR_MAX, UCHAR_MAX };
			D3D11_SUBRESOURCE_DATA subresource_data = {};
			subresource_data.pSysMem = material_color;
			subresource_data.SysMemPitch = 1;
			subresource_data.SysMemSlicePitch = 0;

			ID3D11Texture2D *texture2d;
			hr = device->CreateTexture2D(&texture2d_desc, &subresource_data, &texture2d);
			if (FAILED(hr))
			{
				LOGGER("**********************************\n");
				LOGGER("CreateTexture2D()\n");
				LOGGER("**********************************\n");
				assert(!"Could not Create a dummy texture");
			}

			D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_desc = {};
			shader_resource_view_desc.Format = texture2d_desc.Format;
			shader_resource_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			shader_resource_view_desc.Texture2D.MipLevels = 1;

			hr = device->CreateShaderResourceView(texture2d, &shader_resource_view_desc, srv);
			if (FAILED(hr))
			{
				LOGGER("**********************************\n");
				LOGGER("CreateShaderResourceView()\n");
				LOGGER("**********************************\n");
				assert(!"Could not Create a ShaderResourceView from a dummy texture");
			}


			g_textures.insert(std::make_pair(filename_w, *srv));
			
			texture2d->Release();
		}
	}
	void ReleaseTextures()
	{
		g_textures.clear();
	}

	void Texture2dDescription(ID3D11ShaderResourceView *shader_resource_view, D3D11_TEXTURE2D_DESC &texture2d_desc)
	{
		Microsoft::WRL::ComPtr<ID3D11Resource> resource;
		shader_resource_view->GetResource(resource.GetAddressOf());

		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
		HRESULT hr = resource.Get()->QueryInterface<ID3D11Texture2D>(texture2d.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("**********************************\n");
			LOGGER("CreateShaderResourceView()\n");
			LOGGER("**********************************\n");
			assert(!"Could not Create a ShaderResourceView from a dummy texture");
		}

		texture2d->GetDesc(&texture2d_desc);
	}

	void GetResourcePath(char(&combinedResourcePath)[256], const char* referrer_filename, const char* referent_filename)
	{
		char delimiter[2] = { '/', '\\' };

		std::string path_str(referrer_filename);
		size_t pos = std::wstring::npos;
		for (int i = 0; i < 2; ++i)
		{
			if (pos != std::wstring::npos)
				break;
			pos = path_str.find_last_of(delimiter[i]);
		}
		std::string path = path_str.substr(0, pos + 1);

		pos = std::wstring::npos;

		std::string image_str(referent_filename);
		for (int i = 0; i < 2; ++i)
		{
			if (pos != std::wstring::npos)
				break;
			pos = image_str.find_last_of(delimiter[i]);
		}
		std::string image = image_str.substr(pos + 1, strlen(referent_filename));

		path += image;
		strcpy_s(combinedResourcePath, path.c_str());
	}
}