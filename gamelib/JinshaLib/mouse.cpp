#include "mouse.h"
#include "window.h"


#include <algorithm>


void Mouse::State(HWND hwnd)
{
	GetCursorPos(&cursor);
	ScreenToClient(hwnd, &cursor);

	position.x = (std::max)((std::min)(static_cast<int>(cursor.x), SCREEN_WIDTH), 0);
	position.y = (std::max)((std::min)(static_cast<int>(cursor.y), SCREEN_HEIGHT), 0);

	for (int i = 0; i < 3; ++i)
	{
		previous_state[i] = current_state[i];

		if ((static_cast<unsigned short>(GetAsyncKeyState(buttons[i])) & 0x8000))
		{
			++current_state[i];
		}
		else
		{
			current_state[i] = 0;
		}
	}

	mouse_event(MOUSEEVENTF_WHEEL, 0, 0, 1, 0);
}
bool Mouse::PressedState(const int button)
{
	switch (button)
	{
	case VK_LBUTTON:
		return current_state[0] > 0;
		break;
	case VK_RBUTTON:
		return current_state[1] > 0;
		break;
	case VK_MBUTTON:
		return current_state[2] > 0;
		break;
	default:
		break;
	}
	return false;
}
bool Mouse::RisingState(const int button)
{
	switch (button)
	{
	case VK_LBUTTON:
		return current_state[0] > 0 && previous_state[0] == 0;
		break;
	case VK_RBUTTON:
		return current_state[1] > 0 && previous_state[1] == 0;
		break;
	case VK_MBUTTON:
		return current_state[2] > 0 && previous_state[2] == 0;
		break;
	default:
		break;
	}

	return false;
}
bool Mouse::FallingState(int button)
{
	switch (button)
	{
	case VK_LBUTTON:
		return current_state[0] == 0 && previous_state[0] > 0;
		break;
	case VK_RBUTTON:
		return current_state[1] == 0 && previous_state[1] > 0;
		break;
	case VK_MBUTTON:
		return current_state[2] == 0 && previous_state[2] > 0;
		break;
	default:
		break;
	}

	return false;
}

namespace input
{
	void MouseUpdate(HWND hwnd) { Mouse::GetInstance().State(hwnd); }
	bool MousePressedState(MouseLabel button) { return Mouse::GetInstance().PressedState(static_cast<int>(button)); }
	bool MouseRisingState(MouseLabel button) { return Mouse::GetInstance().RisingState(static_cast<int>(button)); }
	bool MouseFallingState(MouseLabel button) { return Mouse::GetInstance().FallingState(static_cast<int>(button)); }

	int GetCursorPosX() { return Mouse::GetInstance().GetCursorPosX(); };
	int GetCursorPosY() { return Mouse::GetInstance().GetCursorPosY(); };
	long GetWheel()	{ return Mouse::GetInstance().GetWheel(); }
	void SetWheel(long wheel) { Mouse::GetInstance().SetWheel(wheel); }
}