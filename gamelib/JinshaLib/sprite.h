#pragma once
//**************************************
//   include Headers
//**************************************
#include "depth_stencil_state.h"
#include "rasterizer_state.h"
#include "sampler_state.h"
#include "shader.h"
#include "texture.h"
#include "vector.h"

#include <memory>

//**************************************
//   Object class
//**************************************
class Sprite
{
	Microsoft::WRL::ComPtr<ID3D11Buffer>				m_p_vertex_buffer;
	std::unique_ptr<VertexShader>						m_p_vertex_shader;
	std::unique_ptr<PixelShader>						m_p_pixel_shader;
	std::unique_ptr<ShaderResourceView>					m_p_shader_resource_view;
	D3D11_TEXTURE2D_DESC								m_texture2d_desc;
	std::unique_ptr<RasterizerState>					m_p_rasterizer_state;
	std::unique_ptr<SamplerState>						m_p_sampler_state;
	std::unique_ptr<DepthStencilState>					m_p_depth_stencil_state;

	struct Vertex
	{
		VECTOR3F position;
		VECTOR4F color;
		VECTOR2F texcoord;
	};
public:
	Sprite(ID3D11Device* device, const char* filename);
	~Sprite() = default;

	void Render(ID3D11DeviceContext* context, float posX, float posY, float scaleX, float scaleY,
		float texPosX, float texPosY, float texSizeX, float texSizeY, float centerX, float centerY, float angle, float r, float g, float b, float a, bool reverse = false);
	void Render(ID3D11DeviceContext* context, const VECTOR2F& position, const VECTOR2F& scale,
		const VECTOR2F& texPos, const VECTOR2F& texSize, const VECTOR2F& center, float angle, const VECTOR4F& color, bool reverse);
};