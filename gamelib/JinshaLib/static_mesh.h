#pragma once

#include "model_renderer.h"


class StaticMesh
{
	std::unique_ptr<ModelRenderer> model_renderer;
public:
	std::unique_ptr<Model> model;


	StaticMesh(ID3D11Device* device, const char* filename, bool flipping_v_coodinates = false);
	~StaticMesh() = default;

	void Render(ID3D11DeviceContext* device_context,
		const FLOAT4X4& view_projection,
		const VECTOR4F& light,
		const VECTOR4F& color);
};