#pragma once
//**************************************
//   Connect libraries
//**************************************
#ifdef _DEBUG
#ifdef WIN32
#pragma comment( lib, "./External_libraries/DirectXTex/Bin/Desktop_2015/Win32/Debug/DirectXTex.lib")
#else // x64
#pragma comment( lib, "./External_libraries/DirectXTex/Bin/Desktop_2015/x64/Debug/DirectXTex.lib")
#endif
#else // RELEASE
#ifdef WIN32
#pragma comment( lib, "./External_libraries/DirectXTex/Bin/Desktop_2015/Win32/Release/DirectXTex.lib")
#else // x64
#pragma comment( lib, "./External_libraries/DirectXTex/Bin/Desktop_2015/x64/Release/DirectXTex.lib")
#endif
#endif

//**************************************
//   include Headers
//**************************************
#include <DirectXTex.h>
#include <wrl.h>

//**************************************
//   Library methods
//**************************************
namespace JinshaLib
{
	void LoadTextureFromFile(ID3D11Device* device, const char* filename,
		ID3D11ShaderResourceView** srv, bool force_srgb);
	void MakeDummyTexture(ID3D11Device* device, ID3D11ShaderResourceView** srv, unsigned int value, bool force_srgb);
	void ReleaseTextures();

	void Texture2dDescription(ID3D11ShaderResourceView *shader_resource_view, D3D11_TEXTURE2D_DESC &texture2d_desc);
	void GetResourcePath(char(&combinedResourcePath)[256], const char* referrer_filename, const char* referent_filename);
}

//**************************************
//   Object class
//**************************************
class ShaderResourceView
{
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>	m_p_shader_resource_view;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>	m_p_default_shader_resource_view;
public:
	ShaderResourceView(ID3D11Device* device, const char* filename, bool force_srgb)
	{
		JinshaLib::LoadTextureFromFile(device, filename, m_p_shader_resource_view.GetAddressOf(), force_srgb);
	}
	~ShaderResourceView() = default;
	
	void Activate(ID3D11DeviceContext *immediate_context, unsigned int slot)
	{
		immediate_context->PSGetShaderResources(0, 1, m_p_default_shader_resource_view.ReleaseAndGetAddressOf());
		immediate_context->PSSetShaderResources(slot, 1, m_p_shader_resource_view.GetAddressOf());
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->PSSetShaderResources(0, 1, m_p_default_shader_resource_view.ReleaseAndGetAddressOf());
	}

			ID3D11ShaderResourceView* GetSRV()		{ return m_p_shader_resource_view.Get(); }
	const	ID3D11ShaderResourceView* GetSRV() const { return m_p_shader_resource_view.Get(); }
};