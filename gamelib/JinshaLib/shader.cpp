#include "shader.h"
#include "logger.h"

#include <assert.h>

#include <map>
#include <string>

bool LoadFile(const char* filename, char** cso_data, unsigned int& cso_sz)
{
	FILE* fp = nullptr;
	fopen_s(&fp, filename, "rb");

	if (fp == nullptr)
		return false;

	fseek(fp, 0, SEEK_END);
	cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	*cso_data = new char[cso_sz];
	fread(*cso_data, cso_sz, 1, fp);
	fclose(fp);

	return true;
}

namespace JinshaLib
{
	struct VertexShaderAndInputLayout
	{
		VertexShaderAndInputLayout(ID3D11VertexShader  *vertex_shader, ID3D11InputLayout *input_layout) : vertex_shader(vertex_shader), input_layout(input_layout) {}
		Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
	};

	static std::map<std::string, VertexShaderAndInputLayout> g_vertex_shaders;
	HRESULT CreateVertexShader(ID3D11Device *device, const char* filename, ID3D11VertexShader **vertex_shader, ID3D11InputLayout **input_layout, D3D11_INPUT_ELEMENT_DESC *input_element_desc, unsigned int num_elements)
	{
		std::map<std::string, VertexShaderAndInputLayout>::iterator it = g_vertex_shaders.find(filename);
		if (it != g_vertex_shaders.end())
		{
			*vertex_shader = it->second.vertex_shader.Get();
			(*vertex_shader)->AddRef();
			if (input_layout)
			{
				*input_layout = it->second.input_layout.Get();
				(*input_layout)->AddRef();
			}
			return S_OK;
		}

		char* cso_data = nullptr;
		unsigned int cso_sz = 0;
		if (!LoadFile(filename, &cso_data, cso_sz))
			return E_FAIL;

		HRESULT hr = S_OK;
		hr = device->CreateVertexShader(cso_data, cso_sz, 0, vertex_shader);
		if (FAILED(hr))
		{
			LOGGER("********************\n");
			LOGGER("CreateVertexShader()\n");
			LOGGER("********************\n");
			assert(!"Could not CreateVertexShader");
		}

		if (input_layout)
		{
			hr = device->CreateInputLayout(input_element_desc, num_elements, cso_data, cso_sz, input_layout);
			if (FAILED(hr))
			{
				LOGGER("********************\n");
				LOGGER("CreateVertexShader()\n");
				LOGGER("********************\n");
				assert(!"Could not CreateVertexShader");
			}
		}
		delete[] cso_data;

		g_vertex_shaders.insert(std::make_pair(filename, VertexShaderAndInputLayout(*vertex_shader, input_layout ? *input_layout : 0)));

		return hr;
	}
	void ReleaseVertexShaders()
	{
		g_vertex_shaders.clear();
	}

	static std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>> g_pixel_shaders;
	HRESULT CreatePixelShader(ID3D11Device *device, const char *filename, ID3D11PixelShader **pixel_shader)
	{
		std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>::iterator it = g_pixel_shaders.find(filename);
		if (it != g_pixel_shaders.end())
		{
			*pixel_shader = it->second.Get();
			(*pixel_shader)->AddRef();
			return S_OK;
		}

		char* cso_data = nullptr;
		unsigned int cso_sz = 0;
		if (!LoadFile(filename, &cso_data, cso_sz))
			return E_FAIL;

		HRESULT hr = device->CreatePixelShader(cso_data, cso_sz, 0, pixel_shader);
		if (FAILED(hr))
		{
			LOGGER("********************\n");
			LOGGER("CreatePixelShader()\n");
			LOGGER("********************\n");
			assert(!"Could not CreatePixelShader");
		}
		delete[] cso_data;

		g_pixel_shaders.insert(std::make_pair(filename, *pixel_shader));
		return hr;
	}
	void ReleasePixelShaders()
	{
		g_pixel_shaders.clear();
	}
}


//**************************************
//   VertexShader
//**************************************
bool VertexShader::Initialize(ID3D11Device *device, const char *cso, D3D11_INPUT_ELEMENT_DESC *input_element_desc, unsigned int num_elements)
{
	HRESULT hr = JinshaLib::CreateVertexShader(device, cso, m_p_shader.GetAddressOf(), m_p_input_layout.GetAddressOf(), input_element_desc, num_elements);
	if (FAILED(hr))
		return false;

	return true;
}
void VertexShader::Activate(ID3D11DeviceContext *immediate_context)
{
	immediate_context->IAGetInputLayout(m_p_default_input_layout.ReleaseAndGetAddressOf());
	immediate_context->VSGetShader(m_p_default_shader.ReleaseAndGetAddressOf(), 0, 0);

	immediate_context->IASetInputLayout(m_p_input_layout.Get());
	immediate_context->VSSetShader(m_p_shader.Get(), 0, 0);
}
void VertexShader::Deactivate(ID3D11DeviceContext *immediate_context)
{
	immediate_context->IASetInputLayout(m_p_default_input_layout.Get());
	immediate_context->VSSetShader(m_p_default_shader.Get(), 0, 0);
}

//**************************************
//   PixelShader
//**************************************
bool PixelShader::Initialize(ID3D11Device *device, const char *cso)
{
	HRESULT hr = JinshaLib::CreatePixelShader(device, cso, m_p_shader.GetAddressOf());
	if (FAILED(hr))
		return false;

	return true;
}
void PixelShader::Activate(ID3D11DeviceContext* immediate_context)
{
	immediate_context->PSGetShader(m_p_default_shader.ReleaseAndGetAddressOf(), 0, 0);
	immediate_context->PSSetShader(m_p_shader.Get(), 0, 0);
}
void PixelShader::Deactivate(ID3D11DeviceContext* immediate_context)
{
	immediate_context->PSSetShader(m_p_default_shader.Get(), 0, 0);
}