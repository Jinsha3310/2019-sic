//************************************************//
//		include headers
//************************************************//
#include "jinsha_lib.h"
#include "frame_rate.h"
#include "misc.h"
#include "shader.h"
#include "texture.h"
#include "resource.h"
#include "mouse.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif



#include <WRL.h>
#include <vector>
#include <ctime>

struct CoreSystem
{
	HWND m_hwnd;
	HINSTANCE m_hinstance;

	Microsoft::WRL::ComPtr<ID3D11Device>			m_p_device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext>		m_p_immediate_context;
	Microsoft::WRL::ComPtr<IDXGISwapChain>			m_p_swap_chain;
	Microsoft::WRL::ComPtr<IDXGIDevice1>			m_p_dxgi_device;
	Microsoft::WRL::ComPtr<IDXGIAdapter>			m_p_dxgi_adapter;
	Microsoft::WRL::ComPtr<IDXGIFactory>			m_p_dxgi_factory;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	m_p_render_target_view;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView>	m_p_depth_stencil_view;

	JinshaLib::HighResolutionTimer					m_high_resolution_timer;
	JinshaLib::Benchmark							m_benchmark;

	VECTOR4F										m_screen_color;
};
static CoreSystem core;

//*******************************************
//Methods for Creating Device Initialize 
//*******************************************
HRESULT CreateDevice();
HRESULT CreateSwapChain(const int width, const int height, const bool full_screen);
HRESULT CreateRenderTargetView();
HRESULT CreateDepthStencilView(const int width, const int height);
HRESULT DeviceInitialize(HWND hwnd, int width, int height, bool full_screen)
{
	HRESULT hr = S_OK;
	
	hr = CreateDevice();
	if (FAILED(hr))
	{
		assert(!"Could not CreateDevice()");
	}
	hr = CreateSwapChain(width, height, full_screen);
	if (FAILED(hr))
	{
		assert(!"Could not CreateSwapChain()");
	}
	hr = CreateRenderTargetView();
	if (FAILED(hr))
	{
	assert(!"Could not CreateRenderTargetView()");
	}
	hr = CreateDepthStencilView(width, height);
	if (FAILED(hr))
	{
	assert(!"Could not CreateDepthStencilView()");
	}

	return hr;
}
void	DeviceUninitialize()
{
	if (core.m_p_swap_chain.Get())
	{
		core.m_p_swap_chain.Get()->SetFullscreenState(false, nullptr);
	}
}


//*******************************************
//Methods for Creating Window Initialize 
//*******************************************
LRESULT CALLBACK FnWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND WindowInitialize(const char* name, int width, int height, HINSTANCE instance)
{
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(12572);
#endif

	WNDCLASSEXA wcex;
	wcex.cbSize = sizeof(WNDCLASSEXA);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = FnWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = instance;
	wcex.hIcon = LoadIcon(wcex.hInstance, reinterpret_cast<LPCSTR>(IDI_ICON));
	wcex.hCursor = LoadCursorA(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "JinshaLib";
	wcex.hIconSm = LoadIcon(wcex.hInstance, reinterpret_cast<LPCSTR>(IDI_ICON));
	RegisterClassExA(&wcex);

	RECT rc = { 0, 0, width, height };

	const int x = 1920 / 2 - width  / 2;
	const int y = 1080 / 2 - height / 2 ;

	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	HWND hwnd = CreateWindowA("JinshaLib", name, WS_OVERLAPPEDWINDOW ^ WS_MAXIMIZEBOX ^ WS_THICKFRAME | WS_VISIBLE, x, y, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, instance, NULL);
	ShowWindow(hwnd, SW_SHOWDEFAULT);

	return hwnd;
}
void WindowUninitialize()
{
	UnregisterClassA("JinshaLib", core.m_hinstance);
}


/**********************************************/
//Function to build a JinshaLibrary
/**********************************************/
namespace JinshaLib
{
	//Initialize the JinshaLibrary
	bool Initialize(const char* name, const int width, const int height, bool fullscreen, HINSTANCE instance)
	{
		static bool loaded = false;
		if (loaded) return loaded;

		srand((unsigned int)time(NULL));

		core.m_hwnd = WindowInitialize(name, width, height, instance);
		HRESULT hr = DeviceInitialize(core.m_hwnd, width, height, fullscreen);
		if (FAILED(hr))
		{
			assert(!"Could not initialize deivce (JinshaLib)");
			return false;
		}
		core.m_hinstance = instance;
		core.m_high_resolution_timer.Reset();


#ifdef USE_IMGUI
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();

		ImGui_ImplWin32_Init(core.m_hwnd);
		ImGui_ImplDX11_Init(core.m_p_device.Get(), core.m_p_immediate_context.Get());

		ImGui::StyleColorsDark();
#endif

		D3D11_VIEWPORT vp = {};
		vp.Width = (FLOAT)width;
		vp.Height = (FLOAT)height;
		vp.MinDepth = 0.f;
		vp.MaxDepth = 1.f;
		vp.TopLeftX = 0.f;
		vp.TopLeftY = 0.f;

		GetContext()->RSSetViewports(1, &vp);


		UINT m4xMsaaQuality;
		GetDevice()->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);
		assert(m4xMsaaQuality > 0);

		loaded = true;
		return loaded;
	}
	//Uninitialize the JinshaLibrary
	void UnInitialize()
	{
#ifdef USE_IMGUI
		ImGui_ImplDX11_Shutdown();
		ImGui_ImplWin32_Shutdown();
		ImGui::DestroyContext();
#endif

		DeviceUninitialize();
		WindowUninitialize();

		ReleaseVertexShaders();
		ReleasePixelShaders();
		ReleaseTextures();
	}

	void ClearScreen()
	{
		const float color[4] = { core.m_screen_color.x, core.m_screen_color.y, core.m_screen_color.z, core.m_screen_color.w };
		GetContext()->ClearRenderTargetView(core.m_p_render_target_view.Get(), color);
		GetContext()->ClearDepthStencilView(core.m_p_depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
		GetContext()->OMSetRenderTargets(1, core.m_p_render_target_view.GetAddressOf(), core.m_p_depth_stencil_view.Get());
	}
	void ScreenPresent(unsigned int sync_interval, unsigned int flags)
	{
#ifdef USE_IMGUI
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif

		core.m_p_swap_chain.Get()->Present(sync_interval, flags);
	}
	bool GameLoop()
	{
		MSG msg = { 0 };

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) return false;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

#ifdef USE_IMGUI
		//Display frame rate for debugging only
		CalculateFrameStats(core.m_hwnd, &core.m_high_resolution_timer);

		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();
#endif


		core.m_high_resolution_timer.Tick();
		
		


		return true;
	}
	void SetScreenColor(const float r, const float g, const float b)
	{
		core.m_screen_color.x = r;
		core.m_screen_color.y = g;
		core.m_screen_color.z = b;
		core.m_screen_color.w = 1.0f;
	}

	float GetElapsedTime()
	{
		return  core.m_high_resolution_timer.TimeInterval();
	}

	void ResetHighResolutionTimer()
	{
		core.m_high_resolution_timer.Reset();
	}

	float g_bm_timer;
	void RenderingBegin()
	{
		core.m_benchmark.begin();
	}
	void RenderingEnd()
	{
		g_bm_timer = core.m_benchmark.end();
	}
	float GetAverageRenderingTime()
	{
		static const int MAX_SAMPLING = 100;
		static float array_timer[MAX_SAMPLING] = {};
		static int pointer = 0;
		array_timer[pointer] = g_bm_timer;
		pointer = (pointer + 1) % MAX_SAMPLING;
		float sum = 1000;
		for (float& t : array_timer) {
			if (t == 0.f) continue;
			if (sum > t) sum = t;
		}

		return sum;
	}

	ID3D11Device*		 GetDevice()
	{
		return core.m_p_device.Get();
	}
	ID3D11DeviceContext* GetContext()
	{
		return core.m_p_immediate_context.Get();
	}
	HWND				 GetHWND()
	{
		return core.m_hwnd;
	}
	HINSTANCE			 GetHINSTANCE()
	{
		return core.m_hinstance;
	}
}


//****************************
//Window procedure
//****************************
LRESULT CALLBACK FnWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef USE_IMGUI
	if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, wParam, lParam)) return 1;
#endif

	static long mouse_wheel = 0L;
	mouse_wheel = 0L;
	switch (msg) 
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc;
		hdc = BeginPaint(hwnd, &ps);
		EndPaint(hwnd, &ps);
		break;
	}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_CREATE:
		break;
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
		{
			PostMessage(hwnd, WM_CLOSE, 0, 0);
		}
		break;
	case WM_ENTERSIZEMOVE:
		// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
		core.m_high_resolution_timer.Stop();
		break;
	case WM_EXITSIZEMOVE:
		// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
		// Here we reset everything based on the new window dimensions.
		core.m_high_resolution_timer.Start();
		break;
	case WM_MOUSEWHEEL:
		mouse_wheel = GET_WHEEL_DELTA_WPARAM(wParam);
		input::SetWheel(mouse_wheel);
		break;
	case WM_ACTIVATE:
		
		input::SetWheel(mouse_wheel);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

//****************************
//Create Device Initialize
//****************************
Microsoft::WRL::ComPtr<IDXGIFactory> g_dxgi_factory;
HRESULT CreateDevice()
{
	HRESULT hr = S_OK;
	/*hr = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(g_dxgi_factory.GetAddressOf()));
	if (FAILED(hr))
	{
		assert(!"Could not CreateDXGIFactory()");
	}*/

	Microsoft::WRL::ComPtr<IDXGIAdapter> dxgi_adapter;
	hr = CreateDXGIFactory(IID_PPV_ARGS(g_dxgi_factory.GetAddressOf()));
	if (FAILED(hr))
	{
		assert(!"Could not CreateDXGIFactory()");
	}
	hr = g_dxgi_factory->EnumAdapters(0, dxgi_adapter.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"Could not EnumAdapters()");
	}

	DXGI_ADAPTER_DESC dxgi_adapter_desc;
	dxgi_adapter.Get()->GetDesc(&dxgi_adapter_desc);


	UINT create_device_flags = 0;
#if  defined(_DEBUG)
	create_device_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D_DRIVER_TYPE driver_types[] =
	{
		D3D_DRIVER_TYPE_UNKNOWN,
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	
	D3D_FEATURE_LEVEL feature_levels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT num_feature_levels = ARRAYSIZE(feature_levels);

	D3D_FEATURE_LEVEL feature_level;
	for (D3D_DRIVER_TYPE driver_type : driver_types)
	{
		hr = D3D11CreateDevice(dxgi_adapter.Get(), driver_type, NULL, create_device_flags, feature_levels, num_feature_levels, D3D11_SDK_VERSION, core.m_p_device.GetAddressOf(), &feature_level, core.m_p_immediate_context.GetAddressOf());
		if (SUCCEEDED(hr))
			break;
	}

	return hr;
}
HRESULT CreateSwapChain(const int width, const int height, const bool full_screen)
{
	HRESULT hr = S_OK;

	DXGI_SWAP_CHAIN_DESC swap_chain_desc;
	swap_chain_desc.BufferDesc.Width = width;
	swap_chain_desc.BufferDesc.Height = height;
	swap_chain_desc.BufferDesc.RefreshRate.Numerator = 60;
	swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
	swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB; //DXGI_FORMAT_R8G8B8A8_UNORM_SRGB DXGI_FORMAT_R8G8B8A8_UNORM
	swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swap_chain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.OutputWindow = core.m_hwnd;
	swap_chain_desc.BufferCount = 1;
	swap_chain_desc.SampleDesc.Count = 1;
	swap_chain_desc.SampleDesc.Quality = 0;
	swap_chain_desc.Windowed = !full_screen;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swap_chain_desc.Flags = 0;//DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	hr = g_dxgi_factory->CreateSwapChain(core.m_p_device.Get(), &swap_chain_desc, core.m_p_swap_chain.GetAddressOf());
	core.m_p_swap_chain.Get()->SetFullscreenState(full_screen, nullptr);

	return hr;
}
HRESULT CreateRenderTargetView()
{
	Microsoft::WRL::ComPtr<ID3D11Texture2D> backbuffer = nullptr;
	HRESULT hr = core.m_p_swap_chain.Get()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backbuffer);
	if (FAILED(hr))
	{
		assert(!"Could not swapchain's GetBuffer()");
	}

	return core.m_p_device.Get()->CreateRenderTargetView(backbuffer.Get(), nullptr, core.m_p_render_target_view.GetAddressOf());
}
HRESULT CreateDepthStencilView(const int width, const int height)
{
	Microsoft::WRL::ComPtr<ID3D11Texture2D> backbuffer;
	HRESULT hr = S_OK;

	D3D11_TEXTURE2D_DESC depth_stencil_desc;
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
	depth_stencil_desc.Width = width;
	depth_stencil_desc.Height = height;
	depth_stencil_desc.MipLevels = 1;
	depth_stencil_desc.ArraySize = 1;
	depth_stencil_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_stencil_desc.SampleDesc.Count = 1;
	depth_stencil_desc.SampleDesc.Quality = 0;
	depth_stencil_desc.Usage = D3D11_USAGE_DEFAULT;
	depth_stencil_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depth_stencil_desc.CPUAccessFlags = 0;
	depth_stencil_desc.MiscFlags = 0;
	hr = core.m_p_device.Get()->CreateTexture2D(&depth_stencil_desc, NULL, backbuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"Could not CreateTexture2D()");
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depth_stencil_view_desc;
	ZeroMemory(&depth_stencil_view_desc, sizeof(depth_stencil_view_desc));
	depth_stencil_view_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depth_stencil_view_desc.Texture2D.MipSlice = 0;
	hr = core.m_p_device.Get()->CreateDepthStencilView(backbuffer.Get(), &depth_stencil_view_desc, core.m_p_depth_stencil_view.GetAddressOf());
	return hr;
}