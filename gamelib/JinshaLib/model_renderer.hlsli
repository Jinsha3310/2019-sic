struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color    : COLOR;
	float2 texcoord : TEXCOORD;
};

#define MAX_BONES 100


cbuffer CB_Scene : register(b0)
{
	row_major float4x4  view_projection;
	float4				light_direction;
}

cbuffer CB_Mesh : register(b1)
{
	//row_major float4x4 world;
	row_major float4x4	bone_transforms[MAX_BONES];
}

cbuffer CB_Subset : register(b2)
{
	float4				material_color;
}