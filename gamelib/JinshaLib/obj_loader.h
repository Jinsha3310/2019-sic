#pragma once
//**************************************
//   include Headers
//**************************************
#include "model_data.h"

//**************************************
//   Object class
//**************************************
class OBJLoader
{
	char mtl_filename[256];
	std::vector<std::string> newmtl_names;
	std::vector<std::string> usemtl_names;
public:
	bool Load(const char* filename, ModelData& data, bool flipping_v_coodinates);
private:
	bool LoadOBJFile(const char* filename, ModelData& data, bool flipping_v_coodinates);
	bool LoadMTLFile(const char* filename, ModelData& data);
	void CreateBones(ModelData& data);

	int FindMaterialIndex(ModelData& data);
	void CalculateIndexCount(ModelData& data);
};