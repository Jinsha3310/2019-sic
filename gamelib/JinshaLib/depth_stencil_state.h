#pragma once
//**************************************
//   include Headers
//**************************************
#include "logger.h"

#include <d3d11.h>
#include <wrl.h>
#include <assert.h>

//**************************************
//   Object class
//**************************************
class DepthStencilState
{
public:
	DepthStencilState(ID3D11Device *device, BOOL depth_enable = TRUE, D3D11_DEPTH_WRITE_MASK depth_write_mask = D3D11_DEPTH_WRITE_MASK_ALL, D3D11_COMPARISON_FUNC depth_func = D3D11_COMPARISON_LESS)
	{
		D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
		depth_stencil_desc.DepthEnable = depth_enable;
		depth_stencil_desc.DepthWriteMask = depth_write_mask;
		depth_stencil_desc.DepthFunc = depth_func;
		depth_stencil_desc.StencilEnable = FALSE;
		depth_stencil_desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
		depth_stencil_desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
		depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		HRESULT hr = device->CreateDepthStencilState(&depth_stencil_desc, m_p_depth_stencil_state.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("**************************************************\n");
			LOGGER("CreateDepthStencilState error  (DepthStencilState)\n");
			LOGGER("**************************************************\n");
			assert(!"Could not Create DepthStencilState");
		}
	}
	DepthStencilState(ID3D11Device *device, const D3D11_DEPTH_STENCIL_DESC *depth_stencil_desc)
	{
		HRESULT hr = device->CreateDepthStencilState(depth_stencil_desc, m_p_depth_stencil_state.GetAddressOf());
		if(FAILED(hr))
		{
			LOGGER("**************************************************\n");
			LOGGER("CreateDepthStencilState error  (DepthStencilState)\n");
			LOGGER("**************************************************\n");
			assert(!"Could not Create DepthStencilState");
		}
	}
	~DepthStencilState() = default;
	
	void Activate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->OMGetDepthStencilState(m_p_depth_stencil_statedefault_state_object.ReleaseAndGetAddressOf(), 0);
		immediate_context->OMSetDepthStencilState(m_p_depth_stencil_state.Get(), 1);
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->OMSetDepthStencilState(m_p_depth_stencil_statedefault_state_object.Get(), 1);
	}
private:
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_p_depth_stencil_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_p_depth_stencil_statedefault_state_object;
};