#include "fbx_loader.h"
#include "obj_loader.h"
#include "logger.h"

ModelData::ModelData(const char* filename, bool flipping_v_coodinates, const char* extension)
{
	if (PathFileExistsA((std::string(filename) + extension).c_str()))
	{
		// Deserialize from 'fbx_filename + extension' file.
		std::ifstream ifs;
		ifs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(*this);
	}
	else
	{
		std::string filename_extension = PathFindExtensionA(filename);
		if (filename_extension == ".obj")
		{
			OBJLoader obj_loader;
			obj_loader.Load(filename, *this, flipping_v_coodinates);
		}
		else if (filename_extension == ".fbx")
		{
			// Create from 'filename' file.
			//create_from_fbx(filename, triangulate, animation_sampling_rate);
			FBXLoader fbx_loader;
			fbx_loader.Load(filename, *this);
		}
		// Serialize to 'filename + extension' file.
		/*std::ofstream ofs;
		ofs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(*this);*/
	}
}


ModelResource::ModelResource(ID3D11Device* device, std::unique_ptr<ModelData> model_data)
{
	data = std::move(model_data);

	materials.resize(data->materials.size());
	{
		for (size_t material_index = 0; material_index < materials.size(); ++material_index)
		{
			ModelResource::Material& material = materials.at(material_index);
			ModelData::Material& datas_material = data->materials.at(material_index);

			material.color = datas_material.color;
			material.CreateShaderResourceView(device, datas_material.name, true);
		}
	}

	meshes.resize(data->meshes.size());
	{
		for (size_t mesh_index = 0; mesh_index < meshes.size(); ++mesh_index)
		{
			ModelResource::Mesh& mesh = meshes.at(mesh_index);
			ModelData::Mesh& datas_mesh = data->meshes.at(mesh_index);


			mesh.bone_index = datas_mesh.bone_index;
			mesh.bone_indices.resize(datas_mesh.bone_indices.size());
			{
				for (size_t bone_index = 0; bone_index < mesh.bone_indices.size(); ++bone_index)
				{
					mesh.bone_indices.at(bone_index) = datas_mesh.bone_indices.at(bone_index);
				}
			}

			mesh.subsets.resize(datas_mesh.subsets.size());
			{
				for (size_t subset_index = 0; subset_index < mesh.subsets.size(); ++subset_index)
				{
					ModelResource::Subset& subset = mesh.subsets[subset_index];
					ModelData::Subset& datas_subset = datas_mesh.subsets[subset_index];

					subset.index_count = datas_subset.index_count;
					subset.index_start = datas_subset.index_start;

					subset.material = &materials.at(datas_subset.material_index);
				}
			}

			//ボーン変換行列
			mesh.inverse_transforms.resize(datas_mesh.inverse_transforms.size());
			{
				for (size_t transform_index = 0; transform_index < mesh.inverse_transforms.size(); ++transform_index)
				{
					mesh.inverse_transforms.at(transform_index) = datas_mesh.inverse_transforms.at(transform_index);
				}
			}

			//バッファ作成
			mesh.CreateBuffer(device, datas_mesh.vertices.data(), static_cast<unsigned int>(datas_mesh.vertices.size()), datas_mesh.indices.data(), static_cast<unsigned int>(datas_mesh.indices.size()));
			/*datas_mesh.vertices.clear();
			datas_mesh.indices.clear();*/
		}
	}
}

bool ModelResource::Mesh::CreateBuffer(ID3D11Device* device, ModelData::Vertex* vertices, unsigned int vertex_num, unsigned int* indeces, unsigned int index_num)
{
	HRESULT hr = S_OK;
	{
		D3D11_BUFFER_DESC buffer_desc = {};
		D3D11_SUBRESOURCE_DATA subresource_data = {};

		buffer_desc.ByteWidth = sizeof(ModelData::Vertex) * vertex_num;
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		subresource_data.pSysMem = vertices;
		subresource_data.SysMemPitch = 0;
		subresource_data.SysMemSlicePitch = 0;

		hr = device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.ReleaseAndGetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("*******************************\n");
			LOGGER("CreateBuffer error  (ModelData)\n");
			LOGGER("*******************************\n");
			assert(!"Could not Create VertexBuffer");
		}
	}
	{
		D3D11_BUFFER_DESC buffer_desc = {};
		D3D11_SUBRESOURCE_DATA subresource_data = {};

		buffer_desc.ByteWidth = sizeof(unsigned int) * index_num;
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		subresource_data.pSysMem = indeces;
		subresource_data.SysMemPitch = 0;
		subresource_data.SysMemSlicePitch = 0;
		hr = device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.ReleaseAndGetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("*******************************\n");
			LOGGER("CreateBuffer error  (ModelData)\n");
			LOGGER("*******************************\n");
			assert(!"Could not Create IndexBuffer");
		}
	}

	return true;
}
bool ModelResource::Material::CreateShaderResourceView(ID3D11Device *device, std::string& texname, bool force_srgb)
{
	std::function<void(const VECTOR4F &, DWORD &)> convert = [&](const VECTOR4F &colour, DWORD &RGBA)
	{
		DWORD R = static_cast<BYTE>(colour.x * 255);
		DWORD G = static_cast<BYTE>(colour.y * 255);
		DWORD B = static_cast<BYTE>(colour.z * 255);
		DWORD A = static_cast<BYTE>(colour.w * 255);
		RGBA = R | (G << 8) | (B << 16) | (A << 24);
	};

	if (!texname.empty())
	{
		JinshaLib::LoadTextureFromFile(device, texname.c_str(), shader_resource_view.GetAddressOf(), force_srgb);
	}
	else
	{
		DWORD RGBA = 0;
		convert(color, RGBA);
		JinshaLib::MakeDummyTexture(device, shader_resource_view.GetAddressOf(), RGBA, force_srgb);
	}
	return true;
}
