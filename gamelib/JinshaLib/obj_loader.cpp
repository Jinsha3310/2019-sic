//**************************************
//   include Headers
//**************************************
#include "obj_loader.h"
#include "logger.h"


bool OBJLoader::Load(const char* filename, ModelData& data, bool flipping_v_coodinates)
{
	if (!LoadOBJFile(filename, data, flipping_v_coodinates))
	{
		LOGGER("**********************************************\n");
		LOGGER("LoadOBJFile() error  (OBJLoader)\n");
		LOGGER("**********************************************\n");
		assert(!"Could not Load OBJFile  (OBJLoader)");
		return false;
	}

	CalculateIndexCount(data);

	JinshaLib::GetResourcePath(mtl_filename, filename, mtl_filename);
	if (!LoadMTLFile(mtl_filename, data))
	{
		LOGGER("**********************************************\n");
		LOGGER("LoadMTLFile() error  (OBJLoader)\n");
		LOGGER("**********************************************\n");
		assert(!"Could not Load MTLFile  (OBJLoader)");
		return false;
	}

	for (ModelData::Mesh& mesh : data.meshes)
	{
		for (ModelData::Subset& subset : mesh.subsets)
		{
			subset.material_index = FindMaterialIndex(data);
		}
	}

	CreateBones(data);
	
	usemtl_names.clear();

	return true;
}

bool OBJLoader::LoadOBJFile(const char* filename, ModelData& data, bool flipping_v_coodinates)
{
	std::vector<VECTOR3F> positions, normals;
	std::vector<VECTOR2F> texcoords;
	unsigned int current_index = 0;

	std::ifstream fin(filename);
	if (!fin)
		return false;

	char command[256];
	while (fin) {
		fin >> command;
		if (0 == strcmp(command, "mtllib")) {
			fin.ignore();

			char mtl_lib[256];
			fin >> mtl_lib;
			strcpy_s(mtl_filename, mtl_lib);

			ModelData::Mesh current_mesh = {};
			current_mesh.bone_index = 0;
			data.meshes.push_back(current_mesh);
		}
		else if (0 == strcmp(command, "usemtl")) {
			char usemtl[256];
			fin >> usemtl;
			usemtl_names.push_back(usemtl);

			ModelData::Mesh& mesh = data.meshes.back();

			ModelData::Subset current_subset = {};
			current_subset.index_start = static_cast<int>(mesh.indices.size());
			mesh.subsets.push_back(current_subset);

		}
		else if (0 == strcmp(command, "v")) {
			float x, y, z;
			fin >> x >> y >> z;
			positions.push_back(VECTOR3F(x, y, z));
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "vt")) {
			float u, v;
			fin >> u >> v;
			if (flipping_v_coodinates)
				texcoords.push_back(VECTOR2F(u, 1.0f - v));
			else
				texcoords.push_back(VECTOR2F(u, v));

			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "vn")) {
			float i, j, k;
			fin >> i >> j >> k;
			normals.push_back(VECTOR3F(i, j, k));
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "f")) {
			static UINT index = 0;
			for (UINT i = 0; i < 3; i++) {
				ModelData::Vertex vertex;
				UINT v, vt, vn;

				fin >> v;
				vertex.position = positions[v - 1];
				if ('/' == fin.peek()) {
					fin.ignore();
					if ('/' != fin.peek()) {
						fin >> vt;
						vertex.texcoord = texcoords[vt - 1];
					}
					if ('/' == fin.peek()) {
						fin.ignore();
						fin >> vn;
						vertex.normal = normals[vn - 1];
					}
				}

				vertex.bone_index = UVECTOR4(0, 0, 0, 0);
				vertex.bone_weight = VECTOR4F(0.0f, 0.0f, 0.0f, 0.0f);

				data.meshes.back().vertices.push_back(vertex);
				data.meshes.back().indices.push_back(current_index++);
			}
			fin.ignore(1024, '\n');
		}
		else {
			fin.ignore(1024, '\n');
		}
	}
	fin.close();

	return true;
}
bool OBJLoader::LoadMTLFile(const char* filename, ModelData& data)
{
	std::ifstream fin(filename);
	if (!fin)
		return false;

	char command[256];
	static float alpha = 0;
	while (fin)
	{
		fin >> command;
		if (0 == strcmp(command, "#")) {
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "newmtl"))
		{
			char newmtl[256] = { 0 };

			fin >> newmtl;
			newmtl_names.push_back(newmtl);

			data.materials.push_back(ModelData::Material());
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "Ns"))
		{
			//fin >> data.materials.rbegin()->shininess;
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "Tr"))
		{
			fin >> alpha;
			//fin >> data.materials.rbegin()->alpha;
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "Kd"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			data.materials.rbegin()->color = VECTOR4F(r, g, b, 1.0f);
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "Ks"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			//data.materials.rbegin()->specular = VECTOR3F(r, g, b);
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "Ka"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			//data.materials.rbegin()->ambient = VECTOR3F(r, g, b);
			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "map_Kd"))
		{
			char map_Kd[256] = { 0 };

			fin >> map_Kd;
			JinshaLib::GetResourcePath(map_Kd, filename, map_Kd);
			data.materials.rbegin()->name = map_Kd;

			fin.ignore(1024, '\n');
		}
		else if (0 == strcmp(command, "illum")) {
			unsigned int illum;
			fin >> illum;
			//data.materials.rbegin()->illum = illum;
			fin.ignore(1024, '\n');
		}
		else {
			fin.ignore(1024, '\n');
		}
	}
	fin.close();

	return true;
}

void OBJLoader::CreateBones(ModelData& data)
{
	ModelData::Bone bone = {};
	bone.name = "none";
	bone.parent_index = -1;
	bone.scale = VECTOR3F(1.0f, 1.0f, 1.0f);
	bone.rotate = VECTOR4F(0.0f, 0.0f, 0.0f, 0.0f);
	bone.translate = VECTOR3F(0.0f, 0.0f, 0.0f);
	data.bones.push_back(bone);
}

int OBJLoader::FindMaterialIndex(ModelData& data)
{
	for (size_t i = 0; i < usemtl_names.size(); ++i)
	{
		for (std::string& newmtl_name : newmtl_names)
		{
			if (newmtl_name == usemtl_names.at(i))
			{
				newmtl_name = "";
				return static_cast<int>(i);
			}
		}
	}

	return -1;
}

void OBJLoader::CalculateIndexCount(ModelData& data)
{
	std::vector<ModelData::Subset>::reverse_iterator it = data.meshes.back().subsets.rbegin();
	it->index_count = static_cast<int>(data.meshes.back().indices.size()) - it->index_start;
	for (it = data.meshes.back().subsets.rbegin() + 1; it != data.meshes.back().subsets.rend(); ++it) {
		it->index_count = (it - 1)->index_start - it->index_start;
	}
}