//**************************************
//   include Headers
//**************************************
#include "billboard.h"



BillBoard::BillBoard(ID3D11Device *device, const char *filename, bool force_srgb)
{
	HRESULT hr = S_OK;

	//******************************
	//  Create VertexBuffer
	//******************************
	{	
		Vertex vertices[] =
		{
			{ VECTOR3F(-0.5f, +0.5f, 0.0f), VECTOR2F(1, 0) },
			{ VECTOR3F(+0.5f, +0.5f, 0.0f), VECTOR2F(0, 0) },
			{ VECTOR3F(-0.5f, -0.5f, 0.0f), VECTOR2F(1, 1) },
			{ VECTOR3F(+0.5f, -0.5f, 0.0f), VECTOR2F(0, 1) },
		};
		D3D11_BUFFER_DESC vertex_buffer_desc = {};
		vertex_buffer_desc.ByteWidth = sizeof(vertices);
		vertex_buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
		vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertex_buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		vertex_buffer_desc.MiscFlags = 0;
		vertex_buffer_desc.StructureByteStride = 0;
		D3D11_SUBRESOURCE_DATA vertex_subresource_data = {};
		vertex_subresource_data.pSysMem = vertices;
		vertex_subresource_data.SysMemPitch = 0;
		vertex_subresource_data.SysMemSlicePitch = 0;
		hr = device->CreateBuffer(&vertex_buffer_desc, &vertex_subresource_data, m_p_vertex_buffer.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("*********************************\n");
			LOGGER("CreateBuffer() error  (BillBoard)\n");
			LOGGER("*********************************\n");
			assert(!"Could not Create Buffer (BillBoard)");
		}
	}

	//******************************
	//  Create Shaders
	//******************************
	{
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		unsigned int num_elements = ARRAYSIZE(layout);

		bool result = false;
		m_p_vertex_shader = std::make_unique<VertexShader>();
		result = m_p_vertex_shader->Initialize(device, "./Resources/Shaders/billboard_vs.cso", layout, num_elements);
		if (!result)
		{
			LOGGER("*****************************************\n");
			LOGGER("vertex_shader initialize() error (Sprite)\n");
			LOGGER("*****************************************\n");
			assert(!"Could not Initialize vertex_shader");
		}

		m_p_pixel_shader = std::make_unique<PixelShader>();
		result = m_p_pixel_shader->Initialize(device, "./Resources/Shaders/billboard_ps.cso");
		if (!result)
		{
			LOGGER("****************************************\n");
			LOGGER("pixel_shader initialize() error (Sprite)\n");
			LOGGER("****************************************\n");
			assert(!"Could not Initialize pixel_shader");
		}
	}

	//******************************
	//  Create ShaderResourceView
	//******************************
	m_p_shader_resource_view = std::make_unique<ShaderResourceView>(device, filename, force_srgb);
	JinshaLib::Texture2dDescription(m_p_shader_resource_view->GetSRV(), m_texture2d_desc);
	//******************************
	//  Create SamplerState
	//******************************
	D3D11_SAMPLER_DESC sampler_desc;
	{
		sampler_desc.Filter = D3D11_FILTER_ANISOTROPIC;
		sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		sampler_desc.MipLODBias = 0;
		sampler_desc.MaxAnisotropy = 16;
		sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		sampler_desc.BorderColor[0] = 0.0f;
		sampler_desc.BorderColor[1] = 0.0f;
		sampler_desc.BorderColor[2] = 0.0f;
		sampler_desc.BorderColor[3] = 0.0f;
		sampler_desc.MinLOD = 0;
		sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
		m_p_sampler_state = std::make_unique<SamplerState>(device, &sampler_desc);
	}
	//******************************
	//  Create RasterizerState
	//******************************
	D3D11_RASTERIZER_DESC rasterizer_desc = {};
	{
		rasterizer_desc.FillMode = D3D11_FILL_SOLID;
		rasterizer_desc.CullMode = D3D11_CULL_NONE;
		rasterizer_desc.FrontCounterClockwise = FALSE;
		rasterizer_desc.DepthBias = 0;
		rasterizer_desc.DepthBiasClamp = 0;
		rasterizer_desc.SlopeScaledDepthBias = 0;
		rasterizer_desc.DepthClipEnable = FALSE;
		rasterizer_desc.ScissorEnable = FALSE;
		rasterizer_desc.MultisampleEnable = FALSE;
		rasterizer_desc.AntialiasedLineEnable = FALSE;

		m_p_rasterizer_state = std::make_unique<RasterizerState>(device, &rasterizer_desc);
	}
	//******************************
	//  Create DepthStencilState
	//******************************
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc = {};
	{
		depth_stencil_desc.DepthEnable = FALSE;
		depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		depth_stencil_desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
		depth_stencil_desc.StencilEnable = FALSE;
		depth_stencil_desc.StencilReadMask = 0xFF;
		depth_stencil_desc.StencilWriteMask = 0xFF;
		depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		m_p_depth_stencil_state = std::make_unique<DepthStencilState>(device, &depth_stencil_desc);
	}

	m_p_constant_buffer = std::make_unique<ConstantBuffer<Cbuffer>>(device);
}

void BillBoard::Begin(ID3D11DeviceContext* immediate_context)
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	immediate_context->IASetVertexBuffers(0, 1, m_p_vertex_buffer.GetAddressOf(), &stride, &offset);
	immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	m_p_vertex_shader->Activate(immediate_context);
	m_p_pixel_shader->Activate(immediate_context);
	m_p_shader_resource_view->Activate(immediate_context, 0);
	m_p_sampler_state->Activate(immediate_context, 0);
	m_p_rasterizer_state->Activate(immediate_context);
	m_p_depth_stencil_state->Activate(immediate_context);
}
void BillBoard::Render(ID3D11DeviceContext* immediate_context, const FLOAT4X4& projection, const FLOAT4X4& view, const VECTOR3F &position, float scale, const VECTOR4F& color)
{
	//World Matrix
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, T;
		float aspect_ratio = static_cast<float>(m_texture2d_desc.Width / m_texture2d_desc.Height);
		S = DirectX::XMMatrixScaling(scale, scale, scale);
		T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
		W = S * T;
	}
	//View Matrix
	DirectX::XMMATRIX V = DirectX::XMLoadFloat4x4(&view);
	//Projection Matrix
	DirectX::XMMATRIX P = DirectX::XMLoadFloat4x4(&projection);

	//Extract view's transelate component
	FLOAT4X4 inverse_view = view;
	inverse_view._41 = inverse_view._42 = inverse_view._43 = 0.0f;
	inverse_view._44 = 1.0f;
	DirectX::XMMATRIX inverse_view_matrix;
	inverse_view_matrix = DirectX::XMLoadFloat4x4(&inverse_view);
	inverse_view_matrix = DirectX::XMMatrixInverse(nullptr, inverse_view_matrix);

	DirectX::XMStoreFloat4x4(&m_p_constant_buffer->data.world_view_projection, inverse_view_matrix * W * V * P);
	m_p_constant_buffer->data.color = color;
	m_p_constant_buffer->Activate(immediate_context, 0);

	immediate_context->Draw(4, 0);

	m_p_constant_buffer->Deactivate(immediate_context);
}
void BillBoard::End(ID3D11DeviceContext* immediate_context)
{
	m_p_vertex_shader->Deactivate(immediate_context);
	m_p_pixel_shader->Deactivate(immediate_context);
	m_p_shader_resource_view->Deactivate(immediate_context);
	m_p_sampler_state->Deactivate(immediate_context);
	m_p_rasterizer_state->Deactivate(immediate_context);
	m_p_depth_stencil_state->Deactivate(immediate_context);
}