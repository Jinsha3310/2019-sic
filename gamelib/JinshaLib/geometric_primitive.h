#pragma once
//**************************************
//   include Headers
//**************************************
#include "depth_stencil_state.h"
#include "sampler_state.h"
#include "shader.h"
#include "texture.h"
#include "constant_buffer.h"
#include "vector.h"

#include <d3d11.h>
#include <wrl.h>

#include <memory>

//**************************************
//   Object class
//**************************************
class GeometricPrimitive 
{
protected:
	struct Vertex
	{
		VECTOR3F position;
		VECTOR3F normal;
	};

	struct Cbuffer
	{
		FLOAT4X4 world_view_projection;
		FLOAT4X4 world;
		VECTOR4F material_color;
		VECTOR4F light_direction;
	};

	std::unique_ptr<VertexShader>				m_p_vertex_shader;
	std::unique_ptr<PixelShader>				m_p_pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11Buffer>		m_p_vertex_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>		m_p_index_buffer;
	std::unique_ptr<ConstantBuffer<Cbuffer>>	m_p_constant_buffer;
	std::unique_ptr<DepthStencilState>			m_p_depth_stencil_state;

	bool CreateBuffer(ID3D11Device* device, Vertex* vertices, unsigned int vertex_num, unsigned int* indeces, unsigned int index_num);

public:
	GeometricPrimitive(ID3D11Device* device);
	virtual ~GeometricPrimitive();

	void Render(ID3D11DeviceContext* device_context,
		const FLOAT4X4& world_view_projection,
		const FLOAT4X4& world,
		const VECTOR4F& light,
		const VECTOR4F& color);
};

class GeometricCube : public GeometricPrimitive
{
public:
	GeometricCube(ID3D11Device* device);
	~GeometricCube() {};
};

class GeometricCylinder : public GeometricPrimitive
{
public:
	GeometricCylinder(ID3D11Device* device, const unsigned int slices, const float radius);
	~GeometricCylinder() {};
};

class GeometricSphere : public GeometricPrimitive
{
public:
	GeometricSphere(ID3D11Device* device, unsigned int slices = 16, unsigned int stacks = 16);
	~GeometricSphere() {};
};

class GeometricCapsule : public GeometricPrimitive
{
public:
	GeometricCapsule(ID3D11Device* device);
	~GeometricCapsule() {};
};
class GeometricRect : public GeometricPrimitive
{
public:
    GeometricRect(ID3D11Device* device);
    ~GeometricRect() {};
};

class GeometricSphere2 : public GeometricPrimitive
{
private:
    int		numVertices;
    inline Vertex _makeVertex(const DirectX::XMFLOAT3& p);
public:
    //	形状は slices = (div-1) * 4, stacks = (div-1) * 2 と同じ
    GeometricSphere2(ID3D11Device* device, u_int div = 8);
    ~GeometricSphere2() {};

    virtual void render(ID3D11DeviceContext* context,
        const DirectX::XMFLOAT4X4& wvp,
        const DirectX::XMFLOAT4X4& world,
        const DirectX::XMFLOAT4& light_direction,
        const DirectX::XMFLOAT4& material_color = DirectX::XMFLOAT4(1, 1, 1, 1),
        bool bSolid = true);
};