#pragma once
#include <Windows.h>
#include "high_resolution_timer.h"

namespace JinshaLib
{
	//Frame rate is displayed from calculated
	void CalculateFrameStats(HWND, HighResolutionTimer*);
}