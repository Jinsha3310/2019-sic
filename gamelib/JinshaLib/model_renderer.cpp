#include "model_renderer.h"

ModelRenderer::ModelRenderer(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,								D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS",	0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES",		0,	DXGI_FORMAT_R32G32B32A32_UINT,	0,	D3D11_APPEND_ALIGNED_ELEMENT ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	unsigned int num_element = ARRAYSIZE(layout);
	m_p_vertex_shader = std::make_unique<VertexShader>();
	m_p_vertex_shader->Initialize(device, "./Resources/Shaders/model_renderer_vs.cso", layout, num_element);

	m_p_pixel_shader = std::make_unique<PixelShader>();
	m_p_pixel_shader->Initialize(device, "./Resources/Shaders/model_renderer_ps.cso");

	m_p_cb_scene = std::make_unique<ConstantBuffer<CbScene>>(device);
	m_p_cb_mesh = std::make_unique<ConstantBuffer<CbMesh>>(device);
	m_p_cb_subset = std::make_unique<ConstantBuffer<CbSubset>>(device);
}

void ModelRenderer::Begin(ID3D11DeviceContext* immediate_context, const FLOAT4X4& view_projection, const VECTOR4F& light_direction)
{
	m_p_vertex_shader->Activate(immediate_context);
	m_p_pixel_shader->Activate(immediate_context);

	m_p_cb_scene->data.view_projection = view_projection;
	m_p_cb_scene->data.light_direction = light_direction;
	m_p_cb_scene->Activate(immediate_context, 0);
}

void ModelRenderer::Render(ID3D11DeviceContext* immediate_context, const Model& model, const VECTOR4F& color)
{
	unsigned int strides = sizeof(ModelData::Vertex);
	unsigned int offsets = 0;

	for (const ModelResource::Mesh& mesh : model.GetModelResource()->GetMeshes())
	{
		size_t number_of_bones = mesh.bone_indices.size();
		if (number_of_bones > 0)
		{
			for (size_t i = 0; i < number_of_bones; i++)
			{
				//const ModelResource::Bone& bone = model.GetModelResource()->GetBones().at(i);
				const DirectX::XMMATRIX model_w_t = DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_indices.at(i)).world_transform);
				DirectX::XMMATRIX bone_matrix = DirectX::XMLoadFloat4x4(&mesh.inverse_transforms.at(i)) * DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_indices.at(i)).world_transform);
				XMStoreFloat4x4(&m_p_cb_mesh->data.bone_transforms[i], bone_matrix);
			}
		}
		else
		{
			XMStoreFloat4x4(&m_p_cb_mesh->data.bone_transforms[0], DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_index).world_transform));
		}

		m_p_cb_mesh->Activate(immediate_context, 1);
		
		immediate_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &strides, &offsets);
		immediate_context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


		for (const ModelResource::Subset& subset : mesh.subsets)
		{
			m_p_cb_subset->data.material_color = VECTOR4F(subset.material->color.x * color.x, subset.material->color.y * color.y, subset.material->color.z * color.z, subset.material->color.w * color.w);
			m_p_cb_subset->Activate(immediate_context, 2);
			
			immediate_context->PSSetShaderResources(0, 1, subset.material->shader_resource_view.GetAddressOf());
			immediate_context->DrawIndexed(subset.index_count, subset.index_start, 0);
		}
	}
}

void ModelRenderer::End(ID3D11DeviceContext* immediate_context)
{
	m_p_vertex_shader->Deactivate(immediate_context);
	m_p_pixel_shader->Deactivate(immediate_context);
	m_p_cb_scene->Deactivate(immediate_context);
	m_p_cb_mesh->Deactivate(immediate_context);
	m_p_cb_subset->Deactivate(immediate_context);
}