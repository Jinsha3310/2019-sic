#pragma once
//**************************************
//   include Headers
//**************************************
#include "vector.h"


//**************************************
//   Object class
//**************************************
class Camera
{
	VECTOR3F m_eye;
	VECTOR3F m_focus;
	VECTOR3F m_up = VECTOR3F(0.0f, 1.0f, 0.0f);
	
	bool  m_perspective_projection = true;
	float m_fovy_or_view_width = 60;
	float m_aspect_ratio;
	float m_near_z = 0.1f;
	float m_far_z = 1000.0f;

	float m_distance;

	FLOAT4X4 m_view, m_projection;
public:
	Camera(const VECTOR3F& eye, const VECTOR3F& focus, const VECTOR3F& up, bool perspective_projection, float fovy_or_view_width, float aspect_ratio, float near_z, float far_z);
	~Camera() = default;
	
	void Update();
	void Reset(const VECTOR3F& eye, const VECTOR3F& focus, const VECTOR3F& up);

	const VECTOR3F& GetEye() const { return m_eye; }
	const VECTOR3F& GetFocus() const { return m_focus; }
	const VECTOR3F& GetUp() const { return m_up; }
	const FLOAT4X4& GetView() const { return m_view; }
	const FLOAT4X4& GetProjection() const { return m_projection; }
};