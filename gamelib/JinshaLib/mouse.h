#pragma once
#include <Windows.h>

#include "vector.h"
class Mouse
{
	POINT cursor;
	int buttons[3] = { VK_LBUTTON, VK_RBUTTON, VK_MBUTTON };
	int current_state[3];
	int previous_state[3];
	long wheel;
	VECTOR2 position;
public:
	void SetWheel(long mouse_wheel) { wheel = mouse_wheel; }

	int GetCursorPosX() { return position.x; };
	int GetCursorPosY() { return position.y; };
	long GetWheel() { return wheel; }

	void State(HWND hwnd);
	bool PressedState(const int button);
	bool RisingState(const int button);
	bool FallingState(const int button);

	static Mouse& GetInstance()
	{
		static Mouse instance;
		return instance;
	}
};

namespace input
{
	enum class MouseLabel
	{
		LEFT_BUTTON = VK_LBUTTON,
		RIGHT_BUTTON = VK_RBUTTON,
		MID_BUTTON = VK_MBUTTON,
	};

	void MouseUpdate(HWND hwnd);
	bool MousePressedState(MouseLabel button);
	bool MouseRisingState(MouseLabel button);
	bool MouseFallingState(MouseLabel button);

	int GetCursorPosX();
	int GetCursorPosY();
	void SetWheel(long mouse_wheel);
	long GetWheel();
}