#pragma once
//**************************************
//   include Headers
//**************************************
#include <xaudio2.h>

#include <queue>
#include <memory>

//**************************************
//   Object class
//**************************************
class SoundBuffer
{
	struct WAVData
	{
		const WAVEFORMATEX* wfx;
		const unsigned char* startAudio;
		unsigned int audioBytes;
		unsigned int loopStart;
		unsigned int loopLength;
		const unsigned int* seek;       // Note: XMA Seek data is Big-Endian
		unsigned int seekCount;
	};
	std::queue<IXAudio2SourceVoice*> m_p_source_voices;
	std::unique_ptr<uint8_t[]> wavefiles;
	WAVData wavedata;

	bool playing;

	HRESULT FindMediaFile(wchar_t* strDestPath, int cchDest, LPCWSTR strFilename);
	HRESULT LoadWAVAudioFromFile(const wchar_t* szFileName, std::unique_ptr<uint8_t[]>& wavData, WAVData& result);
	HRESULT Initialize(LPCWSTR szFilename);
public:
	SoundBuffer(LPCWSTR szFilename);
	~SoundBuffer() = default;

	HRESULT PlayWave(IXAudio2* pXaudio2, bool loop);
	void Update();
	void Uninitlize();
	bool Playing();
	void Stop();
	void Pause();
	void Volume(float volume);
};