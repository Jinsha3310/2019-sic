#pragma once
#include "model.h"

class SkinnedMesh : public Model
{
public:
	SkinnedMesh(std::shared_ptr<ModelResource> resource) : Model(resource) {};
	~SkinnedMesh() = default;
};