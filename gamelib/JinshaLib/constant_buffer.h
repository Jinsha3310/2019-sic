#pragma once
//****************************
//   include headers 
//****************************
#include "logger.h"

#include <d3d11.h>
#include <wrl.h>
#include <assert.h>

//****************************
//   Object class
//****************************
template<class T>
class ConstantBuffer
{
	unsigned int m_using_slot;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_p_constant_buffer;
public:
	T data;
	ConstantBuffer(ID3D11Device *device)
	{
		/*D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = sizeof(T);
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;*/
		
		D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = sizeof(T);
		buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		HRESULT hr = device->CreateBuffer(&buffer_desc, 0, m_p_constant_buffer.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("*************************************\n");
			LOGGER("Create Buffer error  (ConstantBuffer)\n");
			LOGGER("*************************************\n");
			assert(!"Could not Create ConstantBuffer");
		}
	}
	~ConstantBuffer() = default;

	void Activate(ID3D11DeviceContext *immediate_context, int slot, bool set_in_vs = true, bool set_in_ps = true)
	{
		D3D11_MAP map = D3D11_MAP_WRITE_DISCARD;
		D3D11_MAPPED_SUBRESOURCE mapped_buffer;
		HRESULT hr = immediate_context->Map(m_p_constant_buffer.Get(), 0, map, 0, &mapped_buffer);
		if (FAILED(hr))
		{
			LOGGER("*************************************\n");
			LOGGER("Map error  (ConstantBuffer)\n");
			LOGGER("*************************************\n");
			assert(!"Could not Map (ConstantBuffer)");
		}
		
		memcpy_s(mapped_buffer.pData, sizeof(T), &data, sizeof(T));
		immediate_context->Unmap(m_p_constant_buffer.Get(), 0);

		m_using_slot = slot;

		ID3D11Buffer *null_buffer = 0;
		set_in_vs ? immediate_context->VSSetConstantBuffers(m_using_slot, 1, m_p_constant_buffer.GetAddressOf()) : immediate_context->VSSetConstantBuffers(slot, 1, &null_buffer);
		set_in_ps ? immediate_context->PSSetConstantBuffers(m_using_slot, 1, m_p_constant_buffer.GetAddressOf()) : immediate_context->PSSetConstantBuffers(slot, 1, &null_buffer);
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		ID3D11Buffer *null_buffer = 0;
		immediate_context->VSSetConstantBuffers(m_using_slot, 1, &null_buffer);
		immediate_context->PSSetConstantBuffers(m_using_slot, 1, &null_buffer);
	}

	ID3D11Buffer* GetBuffer() { return m_p_constant_buffer.Get(); }
};