#pragma once
//**************************************
//   include Headers
//**************************************
#include <d3d11.h>
#include <wrl.h>


enum class BLEND_MODE { NONE, ALPHA, ADD, SUBTRACT, REPLACE, MULTIPLY, LIGHTEN, DARKEN, SCREEN, ALPHA_TO_COVERAGE, END };
//**************************************
//   Library Method
//**************************************
namespace JinshaLib
{
	ID3D11BlendState *CreateBlendState(ID3D11Device *device, BLEND_MODE mode);
}

//**************************************
//   Object class
//**************************************
class BlendState
{
public:
	BlendState(ID3D11Device *device, BLEND_MODE mode)
	{
		m_p_blend_state.Attach(JinshaLib::CreateBlendState(device, mode));
	}

	~BlendState() = default;

	void Activate(ID3D11DeviceContext *immediate_context)
	{
		UINT sample_mask = 0xFFFFFFFF;
		immediate_context->OMGetBlendState(m_p_default_state.ReleaseAndGetAddressOf(), 0, &sample_mask);
		immediate_context->OMSetBlendState(m_p_blend_state.Get(), 0, 0xFFFFFFFF);
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->OMSetBlendState(m_p_default_state.Get(), 0, 0xFFFFFFFF);
	}
private:
	Microsoft::WRL::ComPtr<ID3D11BlendState> m_p_blend_state;
	Microsoft::WRL::ComPtr<ID3D11BlendState> m_p_default_state;

};