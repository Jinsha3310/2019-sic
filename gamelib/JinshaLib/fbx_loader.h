#pragma once
//**************************************
//   Connect libraries
//**************************************
#ifdef _DEBUG
#ifdef WIN32
#pragma comment( lib, "./External_libraries/FBX SDK/2016.1.2/lib/vs2015/x86/debug/libfbxsdk-md.lib")
#else // x64
#pragma comment( lib, "./External_libraries/FBX SDK/2016.1.2/lib/vs2015/x64/debug/libfbxsdk-md.lib")
#endif
#else // RELEASE
#ifdef WIN32
#pragma comment( lib, "./External_libraries/FBX SDK/2016.1.2/lib/vs2015/x86/release/libfbxsdk-md.lib")
#else // x64
#pragma comment( lib, "./External_libraries/FBX SDK/2016.1.2/lib/vs2015/x64/release/libfbxsdk-md.lib")
#endif
#endif

//**************************************
//   include Headers
//**************************************
#include "model_data.h"

#include <fbxsdk.h> 

//**************************************
//   Object class
//**************************************
class FBXLoader
{
public:
	bool Load(const char* filename, ModelData& data);
private:
	bool CreateModel(const char* dirname, FbxScene* fbx_scene, ModelData& data);

	void FetchBones(FbxNode* fbx_node, ModelData& data, int parent_node_index);
	void FetchBone(FbxNode* fbx_node, ModelData& data, int parent_node_index);

	void FetchMeshes(FbxNode* fbx_node, ModelData& data);
	void FetchMesh(FbxNode* fbx_node, FbxMesh* fbx_mesh, ModelData& data);

	void FetchMaterials(const char* dirname, FbxScene* fbx_scene, ModelData& data);
	void FetchMaterial(const char* dirname, FbxSurfaceMaterial* fbx_surface_material, ModelData& data);


	void FetchAnimations(FbxScene* fbx_scene, ModelData& data);
	void FetchAnimationTakes(FbxScene* fbx_scene, ModelData& data);

	int FindBoneIndex(ModelData& data, const char* name);
	int FindMaterialIndex(FbxScene* fbx_scene, const FbxSurfaceMaterial* fbx_surface_material);
};