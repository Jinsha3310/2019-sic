#include "fbx_loader.h"
#include "logger.h"

// FbxDouble2 → VEC2
inline VECTOR2F FbxDouble2ToFloat2(const FbxDouble2& fbx_value)
{
	return VECTOR2F(
		static_cast<float>(fbx_value[0]),
		static_cast<float>(fbx_value[1])
	);
}

// FbxDouble3 → VEC3
inline VECTOR3F FbxDouble3ToFloat3(const FbxDouble3& fbx_value)
{
	return VECTOR3F(
		static_cast<float>(fbx_value[0]),
		static_cast<float>(fbx_value[1]),
		static_cast<float>(fbx_value[2])
	);
}

// FbxDouble4 → VEC3
inline VECTOR3F FbxDouble4ToFloat3(const FbxDouble4& fbx_value)
{
	return VECTOR3F(
		static_cast<float>(fbx_value[0]),
		static_cast<float>(fbx_value[1]),
		static_cast<float>(fbx_value[2])
	);
}

// FbxDouble4 → VEC4
inline VECTOR4F FbxDouble4ToFloat4(const FbxDouble4& fbx_value)
{
	return VECTOR4F(
		static_cast<float>(fbx_value[0]),
		static_cast<float>(fbx_value[1]),
		static_cast<float>(fbx_value[2]),
		static_cast<float>(fbx_value[3])
	);
}

// FbxDouble4 → XMFLOAT4x4
inline FLOAT4X4 FbxAMatrixToFloat4x4(const FbxAMatrix& fbx_value)
{
	return FLOAT4X4(
		static_cast<float>(fbx_value[0][0]),
		static_cast<float>(fbx_value[0][1]),
		static_cast<float>(fbx_value[0][2]),
		static_cast<float>(fbx_value[0][3]),
		static_cast<float>(fbx_value[1][0]),
		static_cast<float>(fbx_value[1][1]),
		static_cast<float>(fbx_value[1][2]),
		static_cast<float>(fbx_value[1][3]),
		static_cast<float>(fbx_value[2][0]),
		static_cast<float>(fbx_value[2][1]),
		static_cast<float>(fbx_value[2][2]),
		static_cast<float>(fbx_value[2][3]),
		static_cast<float>(fbx_value[3][0]),
		static_cast<float>(fbx_value[3][1]),
		static_cast<float>(fbx_value[3][2]),
		static_cast<float>(fbx_value[3][3])
	);
}

bool FBXLoader::Load(const char* filename, ModelData& data)
{
	FbxManager* manager = FbxManager::Create();

	// FBXに対する入出力を定義する
	manager->SetIOSettings(FbxIOSettings::Create(manager, IOSROOT));

	// インポータの生成
	FbxImporter* importer = FbxImporter::Create(manager, "");

	bool import_status = false;
	// -1でファイルフォーマット自動判定
	import_status = importer->Initialize(filename, -1, manager->GetIOSettings());
	if (!import_status)
	{
		LOGGER("****************************************************************************\n");
		LOGGER("LoadFBX importer->Initialize(filename, -1, manager->GetIOSettings())\n");
		LOGGER("****************************************************************************\n");
		return false;
	}

	// SceneオブジェクトにFBXファイル内の情報を流し込む
	FbxScene* scene = FbxScene::Create(manager, "scene");

	//sceneにFBXファイルの情報を渡す
	import_status = importer->Import(scene);
	if (!import_status)
	{
		LOGGER("***********************************\n");
		LOGGER("LoadFBX importer->Import(scene)\n");
		LOGGER("***********************************\n");
		return false;
	}
	//シーンのデータを渡したら、解放してOK
	importer->Destroy();

	//ジオメトリを三角形に変換
	FbxGeometryConverter geometry_converter(manager);
	geometry_converter.Triangulate(scene, /*replace*/true);
	//いるんかな？
	geometry_converter.RemoveBadPolygonsFromMeshes(scene);
	
	//モデル構築
	bool result = CreateModel(filename, scene, data);

	//マネージャー解放で、全てのオブジェクト解放
	manager->Destroy();

	return result;
}

bool FBXLoader::CreateModel(const char* filename, FbxScene* fbx_scene, ModelData& data)
{
	FbxNode* fbx_root_node = fbx_scene->GetRootNode();

	FetchBones(fbx_root_node, data, -1);
	FetchMeshes(fbx_root_node, data);
	FetchMaterials(filename, fbx_scene, data);

	FetchAnimationTakes(fbx_scene, data);
	FetchAnimations(fbx_scene, data);

	return true;
}

void FBXLoader::FetchBones(FbxNode* fbx_node, ModelData& data, int parent_node_index)
{
	FbxNodeAttribute* fbx_node_attribute = fbx_node->GetNodeAttribute();
	FbxNodeAttribute::EType fbx_node_attribute_type = FbxNodeAttribute::EType::eUnknown;

	if (fbx_node_attribute != nullptr)
		fbx_node_attribute_type = fbx_node_attribute->GetAttributeType();

	switch (fbx_node_attribute_type)
	{
	case FbxNodeAttribute::eUnknown:
	case FbxNodeAttribute::eNull:
	case FbxNodeAttribute::eMarker:
	case FbxNodeAttribute::eMesh:
	case FbxNodeAttribute::eSkeleton:
		FetchBone(fbx_node,data,parent_node_index);
		break;
	}

	//再帰処理をし、子ノードを処理する
	parent_node_index = static_cast<int>(data.bones.size() - 1);
	for (int i = 0; i < fbx_node->GetChildCount(); ++i)
	{
		FetchBones(fbx_node->GetChild(i), data, parent_node_index);
	}
}
void FBXLoader::FetchBone(FbxNode* fbx_node, ModelData& data, int parent_node_index)
{
	FbxAMatrix& fbx_local_transform = fbx_node->EvaluateLocalTransform();

	ModelData::Bone node;
	node.name = fbx_node->GetName();
	node.parent_index = parent_node_index;
	node.scale = FbxDouble4ToFloat3(fbx_local_transform.GetS());
	node.rotate = FbxDouble4ToFloat4(fbx_local_transform.GetQ());
	node.translate = FbxDouble4ToFloat3(fbx_local_transform.GetT());

	data.bones.push_back(node);
}

void FBXLoader::FetchMeshes(FbxNode* fbx_node, ModelData& data) 
{
	FbxNodeAttribute* fbx_node_attribute = fbx_node->GetNodeAttribute();
	FbxNodeAttribute::EType fbx_node_attribute_type = FbxNodeAttribute::EType::eUnknown;

	if (fbx_node_attribute != nullptr)
	{
		fbx_node_attribute_type = fbx_node_attribute->GetAttributeType();
	}

	switch (fbx_node_attribute_type)
	{
	case FbxNodeAttribute::eMesh:
		FetchMesh(fbx_node, static_cast<FbxMesh*>(fbx_node_attribute), data);
		break;
	}

	//再帰処理をし、子ノードを処理する
	for (int i = 0; i < fbx_node->GetChildCount(); ++i)
	{
		FetchMeshes(fbx_node->GetChild(i), data);
	}
}
void FBXLoader::FetchMesh(FbxNode* fbx_node, FbxMesh* fbx_mesh, ModelData& data)
{
	int fbx_control_points_count = fbx_mesh->GetControlPointsCount();
	int fbx_polygon_vertex_count = fbx_mesh->GetPolygonVertexCount();
	const int* fbx_polygon_vertices = fbx_mesh->GetPolygonVertices();

	int fbx_material_count = fbx_node->GetMaterialCount();
	int fbx_polygon_count = fbx_mesh->GetPolygonCount();

	data.meshes.emplace_back(ModelData::Mesh());
	ModelData::Mesh& mesh = data.meshes.back();
	mesh.indices.resize(fbx_polygon_count * 3);
	mesh.subsets.resize(fbx_material_count > 0 ? fbx_material_count : 1);
	mesh.bone_index = FindBoneIndex(data, fbx_node->GetName());

	//サブセットのマテリアル設定
	for (int fbx_diffuse_index = 0; fbx_diffuse_index < fbx_material_count; ++fbx_diffuse_index)
	{
		const FbxSurfaceMaterial* fbx_surface_material = fbx_node->GetMaterial(fbx_diffuse_index);

		ModelData::Subset& subset = mesh.subsets.at(fbx_diffuse_index);
		subset.material_index = FindMaterialIndex(fbx_node->GetScene(), fbx_surface_material);
	}

	//サブセットのインデックス設定
	if (fbx_material_count > 0)
	{
		for (int fbx_polygon_index = 0; fbx_polygon_index < fbx_polygon_count; ++fbx_polygon_index)
		{
			int fbx_material_index = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(fbx_polygon_index);
			mesh.subsets.at(fbx_material_index).index_count += 3;
		}

		int offset = 0;
		for (ModelData::Subset& subset : mesh.subsets)
		{
			subset.index_start = offset;
			offset += subset.index_count;

			subset.index_count = 0;
		}
	}

	//頂点影響力データ
	struct BoneInfluence
	{
		int use_count = 0;
		int indices[4] = { 0, 0, 0, 0 };
		float weights[4] = { 1.0f, 0.0f, 0.0f, 0.0f };

		void Add(int index, float weight)
		{
			if (use_count < 4)
			{
				indices[use_count] = index;
				weights[use_count] = weight;
			}
			else
			{
				char buffer[64];
				::sprintf_s(buffer, "BoneInfulence:use_count[%d]", use_count);
				::OutputDebugStringA(buffer);
			}
			use_count++;

		}
	};
	//頂点影響力データを抽出する
	std::vector<BoneInfluence> bone_influences;
	{
		bone_influences.resize(fbx_control_points_count);

		FbxAMatrix fbx_geometric_transform(
			fbx_node->GetGeometricTranslation(FbxNode::eSourcePivot),
			fbx_node->GetGeometricRotation(FbxNode::eSourcePivot),
			fbx_node->GetGeometricScaling(FbxNode::eSourcePivot)
		);

		//スキニングに必要な情報を取得する
		int fbx_deformer_count = fbx_mesh->GetDeformerCount(FbxDeformer::eSkin);
		for (int fbx_deformer_index = 0; fbx_deformer_index < fbx_deformer_count; ++fbx_deformer_index)
		{
			FbxSkin* fbx_skin = static_cast<FbxSkin*>(fbx_mesh->GetDeformer(fbx_deformer_index, FbxDeformer::eSkin));

			int fbx_cluster_count = fbx_skin->GetClusterCount();
			for (int fbx_cluster_index = 0; fbx_cluster_index < fbx_cluster_count; ++fbx_cluster_index)
			{
				FbxCluster* fbx_cluster = fbx_skin->GetCluster(fbx_cluster_index);

				//頂点影響力データを抽出する
				{
					int fbx_cluster_control_point_indices_count = fbx_cluster->GetControlPointIndicesCount();
					const int* fbx_control_point_indices = fbx_cluster->GetControlPointIndices();
					const double* fbx_control_point_weights = fbx_cluster->GetControlPointWeights();

					for (int i = 0; i < fbx_cluster_control_point_indices_count; ++i)
					{
						BoneInfluence& bone_influence = bone_influences.at(fbx_control_point_indices[i]);

						bone_influence.Add(fbx_cluster_index, static_cast<float>(fbx_control_point_weights[i]));
						if (bone_influence.use_count >= 4)
						{
							const char* node_name = fbx_node->GetName();
							::OutputDebugStringA(node_name);
						}
					}
				}

				//ボーン変換行列用の逆行列の計算をする
				{
					//メッシュ空間からワールド空間への変換行列
					FbxAMatrix fbx_mesh_space_transform;
					fbx_cluster->GetTransformMatrix(fbx_mesh_space_transform);

					//ボーン空間からワールド空間への変換行列
					FbxAMatrix fbx_bone_space_transform;
					fbx_cluster->GetTransformLinkMatrix(fbx_bone_space_transform);

					//ボーン逆行列を計算する
					const char* fbx_name = fbx_node->GetName();
					FbxAMatrix fbx_inverse_transform = fbx_bone_space_transform.Inverse() * fbx_mesh_space_transform * fbx_geometric_transform;

					FLOAT4X4 inverse_transform = FbxAMatrixToFloat4x4(fbx_inverse_transform);
					mesh.inverse_transforms.emplace_back(inverse_transform);

					int bone_index = FindBoneIndex(data, fbx_cluster->GetLink()->GetName());
					mesh.bone_indices.emplace_back(bone_index);
				}
			}
		}
	}

	//UVセット名
	FbxStringList fbx_uv_names;
	fbx_mesh->GetUVSetNames(fbx_uv_names);

	//頂点データ
	const FbxVector4* fbx_control_points = fbx_mesh->GetControlPoints();
	for (int fbx_polygon_index = 0; fbx_polygon_index < fbx_polygon_count; ++fbx_polygon_index)
	{
		//ポリゴンに適用されているマテリアルインデックスを取得する
		int fbx_material_index = 0;
		if (fbx_material_count > 0)
		{
			fbx_material_index = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(fbx_polygon_index);
		}

		ModelData::Subset& subset = mesh.subsets.at(fbx_material_index);
		const int index_offset = subset.index_start + subset.index_count;

		for (int fbx_vertex_index = 0; fbx_vertex_index < 3; ++fbx_vertex_index)
		{
			ModelData::Vertex vertex;

			int fbx_control_point_index = fbx_mesh->GetPolygonVertex(fbx_polygon_index, fbx_vertex_index);
		
			{
				vertex.position = FbxDouble4ToFloat3(fbx_control_points[fbx_control_point_index]);
			}

			{
				BoneInfluence& bone_influence = bone_influences.at(fbx_control_point_index);
				vertex.bone_index.x = bone_influence.indices[0];
				vertex.bone_index.y = bone_influence.indices[1];
				vertex.bone_index.z = bone_influence.indices[2];
				vertex.bone_index.w = bone_influence.indices[3];
				vertex.bone_weight.x = bone_influence.weights[0];
				vertex.bone_weight.y = bone_influence.weights[1];
				vertex.bone_weight.z = bone_influence.weights[2];
				vertex.bone_weight.w = bone_influence.weights[3];
			}

			if (fbx_mesh->GetElementNormalCount() > 0)
			{
				FbxVector4 fbx_normal;
				fbx_mesh->GetPolygonVertexNormal(fbx_polygon_index, fbx_vertex_index, fbx_normal);
				vertex.normal = FbxDouble4ToFloat3(fbx_normal);
			}
			else
			{
				vertex.normal = VECTOR3F(0, 0, 0);
			}

			if (fbx_mesh->GetElementUVCount() > 0)
			{
				bool fbx_unmapped_uv;
				FbxVector2 fbx_uv;
				fbx_mesh->GetPolygonVertexUV(fbx_polygon_index, fbx_vertex_index, fbx_uv_names[0], fbx_uv, fbx_unmapped_uv);
				fbx_uv[1] = 1.0 - fbx_uv[1];
				vertex.texcoord = FbxDouble2ToFloat2(fbx_uv);
			}
			else
			{
				vertex.texcoord = VECTOR2F(0, 0);
			}

			mesh.indices.at(index_offset + fbx_vertex_index) = static_cast<int>(mesh.vertices.size());
			mesh.vertices.emplace_back(vertex);
		}

		subset.index_count += 3;
	}
}

void FBXLoader::FetchMaterials(const char* filename, FbxScene* fbx_scene, ModelData& data)
{
	int fbx_material_count = fbx_scene->GetMaterialCount();

	if (fbx_material_count > 0)
	{
		for (int fbx_material_index = 0; fbx_material_index < fbx_material_count; ++fbx_material_index)
		{
			FbxSurfaceMaterial* fbx_surface_material = fbx_scene->GetMaterial(fbx_material_index);

			FetchMaterial(filename, fbx_surface_material, data);
		}
	}
	else
	{
		ModelData::Material material;
		material.color = VECTOR4F(1.0f, 1.0f, 1.0f, 1.0f);
		data.materials.emplace_back(material);
	}
}
void FBXLoader::FetchMaterial(const char* filename, FbxSurfaceMaterial* fbx_surface_material, ModelData& data)
{
	ModelData::Material material;

	FbxProperty fbx_diffuse_property = fbx_surface_material->FindProperty(FbxSurfaceMaterial::sDiffuse);
	FbxProperty fbx_diffuse_factor_property = fbx_surface_material->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
	if (fbx_diffuse_property.IsValid() && fbx_diffuse_factor_property.IsValid())
	{
		FbxDouble fbx_factor = fbx_diffuse_factor_property.Get<FbxDouble>();
		FbxDouble3 fbx_color = fbx_diffuse_property.Get<FbxDouble3>();

		material.color.x = static_cast<float>(fbx_color[0] * fbx_factor);
		material.color.y = static_cast<float>(fbx_color[1] * fbx_factor);
		material.color.z = static_cast<float>(fbx_color[2] * fbx_factor);
		material.color.w = 1.0f;
	}

	if (fbx_diffuse_property.IsValid())
	{
		int fbx_texture_count = fbx_diffuse_property.GetSrcObjectCount<FbxFileTexture>();
		if (fbx_texture_count > 0)
		{
			FbxFileTexture* fbx_texture = fbx_diffuse_property.GetSrcObject<FbxFileTexture>();

			char texname[256];
			strcpy_s(texname, fbx_texture->GetRelativeFileName());
			JinshaLib::GetResourcePath(texname, filename, texname);
			material.name = texname;
		}
	}

	data.materials.emplace_back(material);
}

void FBXLoader::FetchAnimations(FbxScene* scene, ModelData& data)
{
	//取得してきたアニメーションを作成
	for (ModelData::AnimationTake& anime_take : data.animation_takes)
	{
		ModelData::Animation animation;

		FbxAnimStack *animation_stack = scene->FindMember<FbxAnimStack>(anime_take.name.c_str());
		scene->SetCurrentAnimationStack(animation_stack);

		FbxTime start_time;
		FbxTime stop_time;
		FbxTakeInfo* take_info = scene->GetTakeInfo(anime_take.name.c_str());
		if (take_info)
		{
			start_time = take_info->mLocalTimeSpan.GetStart();
			stop_time = take_info->mLocalTimeSpan.GetStop();
		}
		else
		{
			FbxTimeSpan time_line_time_span;
			scene->GetGlobalSettings().GetTimelineDefaultTimeSpan(time_line_time_span);

			start_time = time_line_time_span.GetStart();
			stop_time = time_line_time_span.GetStop();
		}

		FbxTime::EMode time_mode = scene->GetGlobalSettings().GetTimeMode();
		FbxTime smapling_step;
		smapling_step.SetTime(0, 0, 1, 0, 0, time_mode);
		smapling_step = static_cast<FbxLongLong>(smapling_step.Get() * (1.0f / anime_take.sampling_rate));
		
		// アニメーションのノードを列挙する
		std::vector<FbxNode*> fbx_nodes;
		std::function<void(FbxNode*)> traverse = [&](FbxNode* fbx_node)
		{
			FbxNodeAttribute* fbx_node_attribute = fbx_node->GetNodeAttribute();
			FbxNodeAttribute::EType fbx_node_attribute_type = FbxNodeAttribute::EType::eUnknown;
			if (fbx_node_attribute != nullptr)
			{
				fbx_node_attribute_type = fbx_node_attribute->GetAttributeType();
			}

			switch (fbx_node_attribute_type)
			{
			case FbxNodeAttribute::eUnknown:
			case FbxNodeAttribute::eNull:
			case FbxNodeAttribute::eMarker:
			case FbxNodeAttribute::eMesh:
			case FbxNodeAttribute::eSkeleton:
				fbx_nodes.emplace_back(fbx_node);
				break;
			}

			for (int i = 0; i < fbx_node->GetChildCount(); i++)
			{
				traverse(fbx_node->GetChild(i));
			}
		};
		traverse(scene->GetRootNode());
		
		animation.keyframes.resize(anime_take.number_of_frames);

		size_t fbx_node_count = fbx_nodes.size();
		ModelData::KeyFrame* keyframe = animation.keyframes.data();
		for (FbxTime time = start_time; time < stop_time; time += smapling_step, ++keyframe)
		{
			keyframe->bone_datas.resize(fbx_node_count);
			for (size_t fbx_node_index = 0; fbx_node_index < fbx_node_count; ++fbx_node_index)
			{
				ModelData::BoneData& key_data = keyframe->bone_datas.at(fbx_node_index);
				FbxNode* fbx_node = fbx_nodes.at(fbx_node_index);

				// 指定時間のローカル行列からスケール値、回転値、移動値を取り出す。
				const FbxAMatrix& fbx_local_transform = fbx_node->EvaluateLocalTransform(time);

				key_data.scale = FbxDouble4ToFloat3(fbx_local_transform.GetS());
				key_data.rotate = FbxDouble4ToFloat4(fbx_local_transform.GetQ());
				key_data.translate = FbxDouble4ToFloat3(fbx_local_transform.GetT());
			}
		}

		data.animations.push_back(animation);
	}
}

void FBXLoader::FetchAnimationTakes(FbxScene* scene, ModelData& data)
{
	//全てのアニメーション名を取得
	FbxArray<FbxString* > array_of_animation_names;
	scene->FillAnimStackNameArray(array_of_animation_names);

	const int number_of_animations = array_of_animation_names.Size();
	for (int index_of_animation = 0; index_of_animation < number_of_animations; ++index_of_animation)
	{
		ModelData::AnimationTake animation_take;
		animation_take.name = array_of_animation_names[index_of_animation]->Buffer();

		FbxAnimStack *animation_stack = scene->FindMember<FbxAnimStack>(array_of_animation_names[index_of_animation]->Buffer());
		if (!animation_stack)
			assert(!"sceneからアニメデータを見つけられませんでした");
		//アニメーションを指定
		scene->SetCurrentAnimationStack(animation_stack);

		//アニメーションデータのサンプリングデータ
		FbxTime::EMode time_mode = scene->GetGlobalSettings().GetTimeMode();
		FbxTime frame_time;
		frame_time.SetTime(0, 0, 0, 1, 0, time_mode);
		animation_take.sampling_rate = static_cast<unsigned int>(frame_time.GetFrameRate(time_mode));
	
		//60フレーム基準でサンプリング
		FbxTime smapling_step;
		smapling_step.SetTime(0, 0, 1, 0, 0, time_mode);
		smapling_step = static_cast<FbxLongLong>(smapling_step.Get() * (1.0f / animation_take.sampling_rate));

		//アニメーションの再生時間を取得
		FbxTakeInfo* take_info = scene->GetTakeInfo(*(array_of_animation_names[index_of_animation]));
		FbxTime start_time;
		FbxTime stop_time;
		if (take_info)
		{
			start_time = take_info->mLocalTimeSpan.GetStart();
			stop_time = take_info->mLocalTimeSpan.GetStop();
		}
		else
		{
			FbxTimeSpan time_line_time_span;
			scene->GetGlobalSettings().GetTimelineDefaultTimeSpan(time_line_time_span);
			start_time = time_line_time_span.GetStart();
			stop_time = time_line_time_span.GetStop();
		}
		//最大フレーム数
		animation_take.number_of_frames = static_cast<int>(((stop_time - start_time) / smapling_step).Get()) + 1;
		data.animation_takes.push_back(animation_take);
	}

	for (int i = 0; i < number_of_animations; i++)
	{
		delete array_of_animation_names[i];
	}
}

int FBXLoader::FindBoneIndex(ModelData& data, const char* name)
{
	for (size_t i = 0; i < data.bones.size(); ++i)
	{
		if (data.bones.at(i).name == name)
		{
			return static_cast<int>(i);
		}
	}

	return -1;
}
int FBXLoader::FindMaterialIndex(FbxScene* fbx_scene, const FbxSurfaceMaterial* fbx_surface_material)
{
	int fbx_material_count = fbx_scene->GetMaterialCount();

	for (int i = 0; i < fbx_material_count; ++i)
	{
		if (fbx_scene->GetMaterial(i) == fbx_surface_material)
		{
			return i;
		}
	}

	return -1;
}