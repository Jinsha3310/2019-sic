#pragma once
//**************************************
//   include Headers
//**************************************
#include <d3d11.h>
#include <wrl.h>

//**************************************
//   Library methods
//**************************************
namespace JinshaLib
{
	HRESULT CreateVertexShader(ID3D11Device *device, const char* filename, ID3D11VertexShader **vertex_shader, ID3D11InputLayout **input_layout, D3D11_INPUT_ELEMENT_DESC *input_element_desc, unsigned int num_elements);
	void ReleaseVertexShaders();

	HRESULT CreatePixelShader(ID3D11Device *device, const char *filename, ID3D11PixelShader **pixel_shader);
	void ReleasePixelShaders();
}

//**************************************
//   Object class
//**************************************
template <class ShaderType>
class BaseShader
{
	Microsoft::WRL::ComPtr<ShaderType> m_p_shader;
	Microsoft::WRL::ComPtr<ShaderType> m_p_default_shader;

public:
	BaseShader() = default;
	~BaseShader() = default;
	bool Initialize(ID3D11Device *device, const char *cso);
	void Activate(ID3D11DeviceContext *immediate_context);
	void Deactivate(ID3D11DeviceContext *immediate_context);

			ShaderType* GetShader()			{ return m_p_shader.Get(); }
	const	ShaderType* GetShader() const	{ return m_p_shader.Get(); }
};

template <>
class BaseShader<ID3D11VertexShader>
{
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	m_p_shader;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	m_p_default_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	m_p_input_layout;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	m_p_default_input_layout;
public:
	BaseShader() = default;
	~BaseShader() = default;
	bool Initialize(ID3D11Device *device, const char *cso, D3D11_INPUT_ELEMENT_DESC *input_element_desc, unsigned int num_elements);
	void Activate(ID3D11DeviceContext *immediate_context);
	void Deactivate(ID3D11DeviceContext *immediate_context);
	

	ID3D11VertexShader* GetShader() { return m_p_shader.Get(); }
	const ID3D11VertexShader* GetShader() const { return m_p_shader.Get(); }
	ID3D11InputLayout* GetInputLayout() { return m_p_input_layout.Get(); }
	const ID3D11InputLayout* GetInputLayout() const { return m_p_input_layout.Get(); }
};

typedef BaseShader<ID3D11VertexShader>	VertexShader;
typedef BaseShader<ID3D11PixelShader>	PixelShader;