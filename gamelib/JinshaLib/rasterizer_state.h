#pragma once
#include "logger.h"

#include <d3d11.h>
#include <wrl.h>
#include <assert.h>


class RasterizerState
{
	Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_p_rasterizer_state;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_p_default_rasterizer_state;
public:
	RasterizerState(ID3D11Device *device, D3D11_FILL_MODE fill_mode = D3D11_FILL_SOLID, D3D11_CULL_MODE cull_mode = D3D11_CULL_BACK, bool front_counter_clockwise = true, bool antialiasing = true, bool depth_clip_enable = true)
	{
		D3D11_RASTERIZER_DESC rasterizer_desc = {};
		rasterizer_desc.FillMode = fill_mode;
		rasterizer_desc.CullMode = cull_mode;
		rasterizer_desc.FrontCounterClockwise = front_counter_clockwise;
		rasterizer_desc.DepthBias = 0;
		rasterizer_desc.DepthBiasClamp = 0.0f;
		rasterizer_desc.SlopeScaledDepthBias = 0.0f;
		rasterizer_desc.DepthClipEnable = static_cast<BOOL>(depth_clip_enable);
		rasterizer_desc.ScissorEnable = FALSE;
		rasterizer_desc.MultisampleEnable = antialiasing && fill_mode == D3D11_FILL_SOLID ? TRUE : FALSE;
		rasterizer_desc.AntialiasedLineEnable = antialiasing ? TRUE : FALSE;
		HRESULT hr = device->CreateRasterizerState(&rasterizer_desc, m_p_rasterizer_state.GetAddressOf());
		
		if (FAILED(hr))
		{
			LOGGER("**********************************************\n");
			LOGGER("CreateRasterizerState error  (RasterizerState)\n");
			LOGGER("**********************************************\n");
			assert(!"Could not Create RasterizerState");
		}
	}
	RasterizerState(ID3D11Device *device, const D3D11_RASTERIZER_DESC *rasterizer_desc)
	{
		HRESULT hr = device->CreateRasterizerState(rasterizer_desc, m_p_rasterizer_state.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("**********************************************\n");
			LOGGER("CreateRasterizerState error  (RasterizerState)\n");
			LOGGER("**********************************************\n");
			assert(!"Could not Create RasterizerState");
		}
	}
	~RasterizerState() = default;

	void Activate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->RSGetState(m_p_default_rasterizer_state.ReleaseAndGetAddressOf());
		immediate_context->RSSetState(m_p_rasterizer_state.Get());
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->RSSetState(m_p_default_rasterizer_state.Get());
	}
};