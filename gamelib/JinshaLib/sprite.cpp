//******************************
//  include Headers
//******************************
#include "sprite.h"


Sprite::Sprite(ID3D11Device* device, const char* filename)
{
	HRESULT hr = S_OK;

	Vertex vertices[4] = 
	{
		{ VECTOR3F(-0.01f, +0.01f, 0), VECTOR4F(1, 1, 1, 1) },
		{ VECTOR3F(+0.01f, +0.01f, 0), VECTOR4F(1, 0, 0, 1) },
		{ VECTOR3F(-0.01f, -0.01f, 0), VECTOR4F(0, 1, 0, 1) },
		{ VECTOR3F(+0.01f, -0.01f, 0), VECTOR4F(0, 0, 1, 1) },
	};

	//******************************
	//  Create VertexBuffer
	//******************************
	D3D11_BUFFER_DESC buffer_desc = {};
	{
		buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
		buffer_desc.ByteWidth = sizeof(vertices);
		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA subresource_data = {};
		subresource_data.pSysMem = vertices;
		subresource_data.SysMemPitch = 0;
		subresource_data.SysMemSlicePitch = 0;

		hr = device->CreateBuffer(&buffer_desc, &subresource_data, m_p_vertex_buffer.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("****************************\n");
			LOGGER("CreateBuffer error  (Sprite)\n");
			LOGGER("****************************\n");
			assert(!"Could not Initialize VertexBuffer");
			return;
		}
	}
	//********************
	//  Create Shaders
	//********************
	{
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR",	  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		unsigned int num_elements = ARRAYSIZE(layout);

		bool result = false;
		m_p_vertex_shader = std::make_unique<VertexShader>();
		result = m_p_vertex_shader->Initialize(device, "./Resources/Shaders/sprite_vs.cso", layout, num_elements);
		if (!result)
		{
			LOGGER("*****************************************\n");
			LOGGER("vertex_shader initialize() error (Sprite)\n");
			LOGGER("*****************************************\n");
			assert(!"Could not Initialize vertex_shader");
		}

		m_p_pixel_shader = std::make_unique<PixelShader>();
		result = m_p_pixel_shader->Initialize(device, "./Resources/Shaders/sprite_ps.cso");
		if (!result)
		{
			LOGGER("****************************************\n");
			LOGGER("pixel_shader initialize() error (Sprite)\n");
			LOGGER("****************************************\n");
			assert(!"Could not Initialize pixel_shader");
		}
	}
	//******************************
	//  Create ShaderResourceView
	//******************************
	m_p_shader_resource_view = std::make_unique<ShaderResourceView>(device, filename, true);
	JinshaLib::Texture2dDescription(m_p_shader_resource_view->GetSRV(), m_texture2d_desc);

	//******************************
	//  Create SamplerState
	//******************************
	D3D11_SAMPLER_DESC sampler_desc;
	sampler_desc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_desc.MipLODBias = 0;
	sampler_desc.MaxAnisotropy = 16;
	sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampler_desc.BorderColor[0] = 0.0f;
	sampler_desc.BorderColor[1] = 0.0f;
	sampler_desc.BorderColor[2] = 0.0f;
	sampler_desc.BorderColor[3] = 0.0f;
	sampler_desc.MinLOD = 0;
	sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
	m_p_sampler_state = std::make_unique<SamplerState>(device, &sampler_desc);
	//******************************
	//  Create RasterizerState
	//******************************
	{
		D3D11_RASTERIZER_DESC rasterizer_desc = {};
		rasterizer_desc.FillMode = D3D11_FILL_SOLID;
		rasterizer_desc.CullMode = D3D11_CULL_NONE;
		rasterizer_desc.FrontCounterClockwise = FALSE;
		rasterizer_desc.DepthBias = 0;
		rasterizer_desc.DepthBiasClamp = 0;
		rasterizer_desc.SlopeScaledDepthBias = 0;
		rasterizer_desc.DepthClipEnable = FALSE;
		rasterizer_desc.ScissorEnable = FALSE;
		rasterizer_desc.MultisampleEnable = FALSE;
		rasterizer_desc.AntialiasedLineEnable = FALSE;

		m_p_rasterizer_state = std::make_unique<RasterizerState>(device, &rasterizer_desc);
	}
	//******************************
	//  Create RasterizerState
	//******************************
	{
		D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
		depth_stencil_desc.DepthEnable = FALSE;
		depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		depth_stencil_desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
		depth_stencil_desc.StencilEnable = FALSE;
		depth_stencil_desc.StencilReadMask = 0xFF;
		depth_stencil_desc.StencilWriteMask = 0xFF;
		depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		m_p_depth_stencil_state = std::make_unique<DepthStencilState>(device, &depth_stencil_desc);
	}
}

void Sprite::Render(ID3D11DeviceContext* immediate_context, float posX, float posY, float scaleX, float scaleY,
	float texPosX, float texPosY, float texSizeX, float texSizeY, float centerX, float centerY, float angle, float r, float g, float b, float a, bool reverse)
{
	Render(immediate_context, VECTOR2F(posX, posY), VECTOR2F(scaleX, scaleY), VECTOR2F(texPosX, texPosY), VECTOR2F(texSizeX, texSizeY), VECTOR2F(centerX, centerY), angle, VECTOR4F(r, g, b, a), reverse);
}
void Sprite::Render(ID3D11DeviceContext* immediate_context, const VECTOR2F& position, const VECTOR2F& scale,
	const VECTOR2F& texPos, const VECTOR2F& texSize, const VECTOR2F& center, float angle, const VECTOR4F& color, bool reverse)
{
	if (scale.x * scale.y == 0.0f) return;

	D3D11_VIEWPORT viewport;
	UINT numViewports = 1;
	immediate_context->RSGetViewports(&numViewports, &viewport);

	float tw = texSize.x;
	float th = texSize.y;
	if (tw <= 0.0f || th <= 0.0f)
	{
		tw = static_cast<float>(m_texture2d_desc.Width);
		th = static_cast<float>(m_texture2d_desc.Height);
	}

	Vertex vertices[] = {
		{ VECTOR3F(-0.0f, +1.0f, 0), color, VECTOR2F(0, 1) },
		{ VECTOR3F(+1.0f, +1.0f, 0), color, VECTOR2F(1, 1) },
		{ VECTOR3F(-0.0f, -0.0f, 0), color, VECTOR2F(0, 0) },
		{ VECTOR3F(+1.0f, -0.0f, 0), color, VECTOR2F(1, 0) },
	};

	float sinValue = sinf(angle);
	float cosValue = cosf(angle);
	float mx = (tw * scale.x) / tw * center.x;
	float my = (th * scale.y) / th * center.y;


	float tex_mirror = 0.0f;
	for (int i = 0; i < 4; i++) {
		vertices[i].position.x *= (tw * scale.x);
		vertices[i].position.y *= (th * scale.y);

		vertices[i].position.x -= mx;
		vertices[i].position.y -= my;

		float rx = vertices[i].position.x;
		float ry = vertices[i].position.y;
		vertices[i].position.x = rx * cosValue - ry * sinValue;
		vertices[i].position.y = rx * sinValue + ry * cosValue;

		vertices[i].position.x += mx;
		vertices[i].position.y += my;

		vertices[i].position.x += (position.x - scale.x * center.x);
		vertices[i].position.y += (position.y - scale.y * center.y);

		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;
		
		if (reverse)
			tex_mirror = 1.0f;

		vertices[i].texcoord.x = (texPos.x + vertices[i].texcoord.x * tw) / m_texture2d_desc.Width + tex_mirror;
		vertices[i].texcoord.y = (texPos.y + vertices[i].texcoord.y * th) / m_texture2d_desc.Height;
	}

	D3D11_MAPPED_SUBRESOURCE mapped_buffer;
	HRESULT hr = immediate_context->Map(m_p_vertex_buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_buffer);
	if (FAILED(hr))
	{
		LOGGER("*************************************\n");
		LOGGER("Map error  (Sprite)\n");
		LOGGER("*************************************\n");
		assert(!"Could not Map (Sprite)");
	}
	
	memcpy(mapped_buffer.pData, vertices, sizeof(vertices));
	immediate_context->Unmap(m_p_vertex_buffer.Get(), 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	immediate_context->IASetVertexBuffers(0, 1, m_p_vertex_buffer.GetAddressOf(), &stride, &offset);
	immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	m_p_vertex_shader->Activate(immediate_context);
	m_p_pixel_shader->Activate(immediate_context);
	m_p_shader_resource_view->Activate(immediate_context, 0);
	m_p_rasterizer_state->Activate(immediate_context);
	m_p_depth_stencil_state->Activate(immediate_context);
	m_p_sampler_state->Activate(immediate_context, 0);
	immediate_context->Draw(4, 0);
	m_p_vertex_shader->Deactivate(immediate_context);
	m_p_pixel_shader->Deactivate(immediate_context);
	m_p_shader_resource_view->Deactivate(immediate_context);
	m_p_rasterizer_state->Deactivate(immediate_context);
	m_p_depth_stencil_state->Deactivate(immediate_context);
	m_p_sampler_state->Deactivate(immediate_context);
}
