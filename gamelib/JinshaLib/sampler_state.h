#pragma once
#include "logger.h"

#include <d3d11.h>
#include <wrl.h>
#include <assert.h>


class SamplerState
{
public:
	SamplerState(ID3D11Device *device, D3D11_FILTER filter = D3D11_FILTER_ANISOTROPIC, D3D11_TEXTURE_ADDRESS_MODE address_mode = D3D11_TEXTURE_ADDRESS_WRAP, D3D11_COMPARISON_FUNC comparison_func = D3D11_COMPARISON_NEVER, float r = 1, float g = 1, float b = 1, float a = 1)
	{
		D3D11_SAMPLER_DESC sampler_desc = {};
		sampler_desc.Filter = filter;
		sampler_desc.AddressU = address_mode;
		sampler_desc.AddressV = address_mode;
		sampler_desc.AddressW = address_mode;
		sampler_desc.MipLODBias = 0;
		sampler_desc.MaxAnisotropy = 16;
		sampler_desc.ComparisonFunc = comparison_func;
		float border_color[4] = { r, g, b, a };
		memcpy(sampler_desc.BorderColor, border_color, sizeof(float) * 4);
		sampler_desc.MinLOD = 0;
		sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
		HRESULT hr = device->CreateSamplerState(&sampler_desc, m_p_sampler_state.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("****************************************\n");
			LOGGER("CreateSamplerState error  (SamplerState)\n");
			LOGGER("****************************************\n");
			assert(!"Could not Create SamplerState");
		}
	}
	SamplerState(ID3D11Device *device, const D3D11_SAMPLER_DESC *sampler_desc)
	{
		HRESULT hr = device->CreateSamplerState(sampler_desc, m_p_sampler_state.GetAddressOf());
		if (FAILED(hr))
		{
			LOGGER("****************************************\n");
			LOGGER("CreateSamplerState error  (SamplerState)\n");
			LOGGER("****************************************\n");
			assert(!"Could not Create SamplerState");
		}
	}
	~SamplerState() = default;

	void Activate(ID3D11DeviceContext *immediate_context, unsigned int slot, bool set_in_vs = false)
	{
		m_using_slot = slot;
		immediate_context->PSGetSamplers(m_using_slot, 1, m_p_default_sampler_state[0].ReleaseAndGetAddressOf());
		immediate_context->VSSetSamplers(m_using_slot, 1, m_p_sampler_state.GetAddressOf());

		immediate_context->PSSetSamplers(m_using_slot, 1, m_p_sampler_state.GetAddressOf());
		if (set_in_vs)
		{
			immediate_context->VSGetSamplers(m_using_slot, 1, m_p_default_sampler_state[1].ReleaseAndGetAddressOf());
		}
	}
	void Deactivate(ID3D11DeviceContext *immediate_context)
	{
		immediate_context->PSSetSamplers(m_using_slot, 1, m_p_default_sampler_state[0].GetAddressOf());
		immediate_context->VSSetSamplers(m_using_slot, 1, m_p_default_sampler_state[1].GetAddressOf());
	}
private:
	unsigned int m_using_slot = 0;
	Microsoft::WRL::ComPtr<ID3D11SamplerState> m_p_sampler_state;
	Microsoft::WRL::ComPtr<ID3D11SamplerState> m_p_default_sampler_state[2];
};