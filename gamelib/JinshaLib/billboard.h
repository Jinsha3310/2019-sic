#pragma once
//**************************************
//   include Headers
//**************************************
#include "depth_stencil_state.h"
#include "sampler_state.h"
#include "rasterizer_state.h"
#include "shader.h"
#include "texture.h"
#include "constant_buffer.h"
#include "vector.h"

#include <memory>

//**************************************
//   Object class
//**************************************
class BillBoard
{
	struct Vertex
	{
		VECTOR3F position;
		VECTOR2F texcoord;
	};
	struct Cbuffer
	{
		FLOAT4X4 world_view_projection;
		VECTOR4F color;
	};

	std::unique_ptr<VertexShader>			m_p_vertex_shader;
	std::unique_ptr<PixelShader>			m_p_pixel_shader;
	D3D11_TEXTURE2D_DESC					m_texture2d_desc;
	std::unique_ptr<ShaderResourceView>		m_p_shader_resource_view;
	std::unique_ptr<SamplerState>			m_p_sampler_state;
	std::unique_ptr<RasterizerState>		m_p_rasterizer_state;
	std::unique_ptr<DepthStencilState>		m_p_depth_stencil_state;
	Microsoft::WRL::ComPtr<ID3D11Buffer>	m_p_vertex_buffer;
	std::unique_ptr<ConstantBuffer<Cbuffer>>m_p_constant_buffer;

public:
	BillBoard(ID3D11Device *device, const char *filename, bool force_srgb);
	~BillBoard() = default;

	void Begin(ID3D11DeviceContext* immediate_context);
	void Render(ID3D11DeviceContext* immediate_context, const FLOAT4X4& projection, const FLOAT4X4& view, const VECTOR3F &position,  float scale, const VECTOR4F& colour);
	void End(ID3D11DeviceContext* immediate_context);
};