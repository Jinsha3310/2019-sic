#include "keyboard.h"

namespace input
{
	void KeyboardUpdate() { KeyboardManager::GetInstance().Update(); };
	bool KeyboardPressedState(KeyLabel key) { return KeyboardManager::GetInstance().PressedState(static_cast<int>(key)); }
	bool KeyboardRisingState(KeyLabel key) { return KeyboardManager::GetInstance().RisingState(static_cast<int>(key)); }
	bool KeyboardFallingState(KeyLabel key) { return KeyboardManager::GetInstance().FallingState(static_cast<int>(key)); }
}