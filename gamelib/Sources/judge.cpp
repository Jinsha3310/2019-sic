#include "judge.h"
#include "player.h"

#define OBJ_RADIUS 10.0f
#define PLAYER_ATTACK_RADIUS 7.0f

void Judge()
{
	if (pPlayer.IsAttackMode())
	{
		const float degree_80 = 80.0f * 0.01745f;
		VECTOR3F player_pos_min = VECTOR3F(pPlayer.GetPositionX() + sinf(pPlayer.GetRotateY()*-degree_80), pPlayer.GetPositionY() + 3.0f, pPlayer.GetPositionZ() + cosf(pPlayer.GetRotateY()*-degree_80));
		VECTOR3F player_pos_max = VECTOR3F(pPlayer.GetPositionX() + sinf(pPlayer.GetRotateY()*degree_80), pPlayer.GetPositionY() + 3.0f, pPlayer.GetPositionZ() + cosf(pPlayer.GetRotateY()*degree_80));
		if (IsHitCapsuleSphere(
			player_pos_min,
			player_pos_max,
			VECTOR3F(0, 3, 0),
			PLAYER_ATTACK_RADIUS, OBJ_RADIUS))
		{
			pPlayer.hit = true;
		}
	}
}