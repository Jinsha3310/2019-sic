#pragma once

constexpr char* WINDOW_TEXT = "JinshaLibrary";
constexpr int SCREEN_WIDTH  = 900;
constexpr int SCREEN_HEIGHT	= 900;

constexpr bool WINDOW_SCREEN = false;
constexpr bool FULL_SCREEN	 = true;
constexpr bool WINDOW_MODE	 = WINDOW_SCREEN;