#include "stagemanager.h"
#include"vectordelete.h"
#include"window.h"
#include"keyboard.h"
#include"collision.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
#include<ImgUtil.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

//"./Resources/FBX/break_assetPrane.fbm/break_assetPrane.fbx",:地面
void StageManager::Initialize(ID3D11Device * device)
{
	static const char*Filename[]
	{
		"./Resources/FBX/break_assetDesk.fbx",
		"./Resources/FBX/break_assetPrant.fbx",
		"./Resources/FBX/break_assetVault.fbx",
		"./Resources/FBX/break_assetPrinter.fbx",
	};
	data.resize(4);
	model_resource.resize(data.size());
	for (int i=0;i<data.size();i++)
	{
		data[i] = std::make_unique<ModelData>(Filename[i]);
		model_resource[i] = std::make_shared<ModelResource>(device, std::move(data[i]));
	}
#ifdef USE_IMGUI
	model_data = std::make_unique<ModelData>("./Resources/FBX/000_cube.fbx");
	model_r = std::make_shared<ModelResource>(device, std::move(model_data));
	model = std::make_unique<Model>(model_r);
	hit_area_model = std::make_unique<Model>(model_r);
	pos = VECTOR3F(0.f, 0.f, 0.f);
	rotate = VECTOR3F(0.f, 0.f, 0.f);
	scale = VECTOR3F(1.f, 1.f, 1.f);
#endif	
	renderer = std::make_unique<ModelRenderer>(device);
	de = device;
	LoadData();
}
//ステージデータの読み込み
void StageManager::LoadData()
{
	std::vector<SaveData>savedata;
	FILE*fp;
	int size = 0;
	if (fopen_s(&fp, "./Resources/filedata/stage.bin", "rb")==0)
	{
		fread(&size, sizeof(int), 1, fp);
		if (size > 0)
		{
			b_obj.resize(size);
			savedata.resize(size);
			fread(&savedata[0], sizeof(SaveData), size, fp);
			for (int i = 0; i < size; i++)
			{
				b_obj[i].Initialize(de,model_resource[savedata[i].type], savedata[i].type, savedata[i].postion, savedata[i].rotate);
				b_obj[i].Setsize(savedata[i].min, savedata[i].max);
			}
			savedata.clear();
		}
		fclose(fp);
	}
}
//ステージデータの保存
void StageManager::Save()
{
	std::vector<SaveData>savedata;
	FILE*fp;
	int size = b_obj.size();
	fopen_s(&fp, "./Resources/filedata/stage.bin", "wb");
	fwrite(&size, sizeof(int), 1, fp);
	if (size > 0)
	{
		savedata.resize(size);
		for (int i = 0; i < size; i++)
		{
			savedata[i].type = b_obj[i].type;
			savedata[i].postion = b_obj[i].position;
			savedata[i].rotate = b_obj[i].rotate;
			savedata[i].min = b_obj[i].GetMin();
			savedata[i].max = b_obj[i].GetMax();
		}
		fwrite(&savedata[0], sizeof(SaveData), size, fp);
		savedata.clear();
	}

	fclose(fp);

}

void StageManager::Updata(float elapsed_time)
{

#ifdef USE_IMGUI
	static int num;
	static bool hit_area_set = false;
	static const char* MapobjName[]
	{
		"break_assetDesk","break_assetPrant","break_assetVault","break_assetPrinter"
	};
	ImGui::Begin("StageEditor");
	ImGui::Combo("brakobj",&num,MapobjName, (int)(sizeof(MapobjName) / sizeof(*MapobjName)));
	ImGui::Text("brakobj:%d", b_obj.size());
	//配置前
	if (!hit_area_set)
	{
		ImGui::Text("x key:rotation");
		ImGui::Text("z key:set");
		ImGui::Text("c key:delete");
		ImGui::Text("up,down,left,right key:move");
		Move_SetPos();
		//配置
		if (input::KeyboardRisingState(input::KeyLabel::Z))
		{
			b_obj.emplace_back();
			b_obj.back().Initialize(de, model_resource[num], num, pos, rotate);
			hit_area_set = true;
		}
		//消去
		if (input::KeyboardRisingState(input::KeyLabel::C))
		{
			bool hit = false;
			VECTOR3F min = VECTOR3F(pos.x - 2.f, pos.y, pos.z - 2.f);
			VECTOR3F max = VECTOR3F(pos.x + 2.f, pos.y+2.f, pos.z + 2.f);
			for (auto&obj : b_obj)
			{
				hit = IsHitCube(obj.GetMin(), obj.GetMax(), min, max);
				if (hit)
				{
					obj.SetFlag(false);
					break;
				}
			}
		}
		//セーブ
		if (ImGui::Button("save"))
		{
			Save();
		}
	}
	//配置後
	else
	{
		//当たり判定決め
		ImGui::SliderFloat("hit_areaX", &scale.x, 0.f, 30.f);
		ImGui::SliderFloat("hit_areay", &scale.y, 0.f, 30.f);
		ImGui::SliderFloat("hit_areaz", &scale.z, 0.f, 30.f);
		//終了
		if (ImGui::Button("end"))
		{
			b_obj.back().SetHitArea(scale);
			hit_area_set = false;
			scale = VECTOR3F(1.f, 1.f, 1.f);
		}
	}
	ImGui::End();
#endif
	for (auto&obj : b_obj)
		if(obj)obj.Updata(elapsed_time);

	Vectordelete(b_obj);
}
//オブジェクトの配置場所や向き
void StageManager::Move_SetPos()
{
	if (input::KeyboardPressedState(input::KeyLabel::UP))
	{
		pos.z -= 0.1f;
	}
	if (input::KeyboardPressedState(input::KeyLabel::DOWN))
	{
		pos.z += 0.1f;
	}
	if (input::KeyboardPressedState(input::KeyLabel::RIGHT))
	{
		pos.x += 0.1f;
	}
	if (input::KeyboardPressedState(input::KeyLabel::LEFT))
	{
		pos.x -= 0.1f;
	}
	if (input::KeyboardRisingState(input::KeyLabel::X))
	{
		static int angle_num = 0;
		angle_num++;
		if (angle_num == 4)angle_num = 0;
		rotate.y = 3.14*0.5f*angle_num;
	}

}

void StageManager::Render(ID3D11DeviceContext * immediate_context, const FLOAT4X4&view_projection, const VECTOR4F&light_direction)
{
	for (auto&obj : b_obj)
	{
		obj.Render(immediate_context, view_projection, light_direction);
	}
#ifdef USE_IMGUI
	//配置場所の表示
	DirectX::XMMATRIX S, R, T;
	S = DirectX::XMMatrixScaling(1.0f, 0.1f, 1.0f);
	R = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);
	T = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
	DirectX::XMMATRIX W;
	W = S * R * T;
	model->CalculateLocalTransform();
	model->CalculateWorldTransform(W);

	renderer->Begin(immediate_context, view_projection, light_direction);
	renderer->Render(immediate_context, *model, VECTOR4F(1, 1, 1, 1));
	renderer->End(immediate_context);
	//当たり判定表示
	VECTOR3F s = scale*0.5f;
	T = DirectX::XMMatrixTranslation(pos.x, pos.y+s.y, pos.z);

	S = DirectX::XMMatrixScaling(s.x, s.y, s.z);
	W = S * R * T;
	
	hit_area_model->CalculateLocalTransform();
	hit_area_model->CalculateWorldTransform(W);

	renderer->Begin(immediate_context, view_projection, light_direction);
	renderer->Render(immediate_context, *hit_area_model, VECTOR4F(1, 1, 1, 0.5f));
	renderer->End(immediate_context);

#endif
}
