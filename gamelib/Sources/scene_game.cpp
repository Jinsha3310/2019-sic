#include "scene_game.h"
#include "keyboard.h"
#include "model_renderer.h"
#include "rasterizer_state.h"
#include "depth_stencil_state.h"
#include "blend_state.h"
#include "sampler_state.h"
#include "sound.h"
#include "gamepad.h"
#include "billboard.h"
#include "geometric_primitive.h"
#include "mouse.h"
#include "sprite.h"
#include"stagemanager.h"
//game object include
#include "player.h"
#include "collision.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif



std::unique_ptr<BillBoard> board;

std::unique_ptr<RasterizerState> rs;
std::unique_ptr<BlendState> bs;
std::unique_ptr<DepthStencilState>dss;
std::unique_ptr<SamplerState> ss;

std::unique_ptr<Sound> sound;

using namespace JinshaLib;
bool SceneGame::Initialize(ID3D11Device* device, int width, int height)
{
	rs = std::make_unique<RasterizerState>(device, D3D11_FILL_SOLID, D3D11_CULL_BACK, true, false, true);
	bs = std::make_unique<BlendState>(device, BLEND_MODE::ALPHA);
	ss = std::make_unique<SamplerState>(device, D3D11_FILTER_MIN_MAG_MIP_POINT);
	dss = std::make_unique<DepthStencilState>(device, true, D3D11_DEPTH_WRITE_MASK_ALL, D3D11_COMPARISON_LESS);

	sound = std::make_unique<Sound>("./Resources\\Songs\\MusicMono.wav");

	loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);
	
		std::unique_ptr<ModelData> data = std::make_unique<ModelData>("./Resources/FBX/anim_data.fbm/anim_data.fbx");
		std::shared_ptr<ModelResource> model_resource;
		model_resource = std::make_shared<ModelResource>(device, std::move(data));
		if (!pPlayer.Exist())
		{
			pPlayer.Create(device, model_resource);
			pPlayer.Initialize(VECTOR3F(10, 0, 0), VECTOR3F(0, 0, 0));
		}
		pStageManager.Initialize(device);
	}, device);

	if (pPlayer.Exist())
		pPlayer.Initialize(VECTOR3F(10, 0, 0), VECTOR3F(0, 0, 0));
	
	m_camera = std::make_unique<Camera>(VECTOR3F(30.0f, 30.0f, 50.0f), VECTOR3F(0.0f, 0.0f, 0.0f), VECTOR3F(0.0f, 1.0f, 0.0f), true, 30.0f, static_cast<float>(width) / height, 0.1f, 1000.0f);
	
	board = std::make_unique<BillBoard>(device, "Resources/Images/blueeffect.png", true);

	input::GamepadInitialize(0, false, 0.18f, 0.18f);
	return true;
}

void SceneGame::Uninitialize()
{
	pPlayer.Destory();

	EndLoading();
}

static bool t = true;

void SceneGame::Update(float elapsed_time)
{
	if (IsNowLoading())
	{
		return ;
	}
	EndLoading();

#ifdef USE_IMGUI
	ImGui::Begin("GamePad0");

	ImGui::Text("a::%d", input::ButtonPressedState(0, input::PadLabel::A));
	ImGui::Text("b::%d", input::ButtonPressedState(0, input::PadLabel::B));
	ImGui::Text("x::%d", input::ButtonPressedState(0, input::PadLabel::X));
	ImGui::Text("y::%d", input::ButtonPressedState(0, input::PadLabel::Y));

	ImGui::Text("up::%d", input::ButtonPressedState(0, input::PadLabel::UP));
	ImGui::Text("down::%d", input::ButtonPressedState(0, input::PadLabel::DOWN));
	ImGui::Text("left::%d", input::ButtonPressedState(0, input::PadLabel::LEFT));
	ImGui::Text("right::%d", input::ButtonPressedState(0, input::PadLabel::RIGHT));

	ImGui::Text("start::%d", input::ButtonPressedState(0, input::PadLabel::START));
	ImGui::Text("back::%d", input::ButtonPressedState(0, input::PadLabel::BACK));

	ImGui::Text("RSHOULDER::%d", input::ButtonPressedState(0, input::PadLabel::RSHOULDER));
	ImGui::Text("LSHOULDER::%d", input::ButtonPressedState(0, input::PadLabel::LSHOULDER));
	ImGui::Text("RTHUMB::%d", input::ButtonPressedState(0, input::PadLabel::RTHUMB));
	ImGui::Text("LTHUMB::%d", input::ButtonPressedState(0, input::PadLabel::LTHUMB));


	ImGui::Text("Rtriffer::%d", input::TriggerPressedState(0, true));
	ImGui::Text("Ltriffer::%d", input::TriggerPressedState(0, false));

	ImGui::Text("RX STICK::%f", input::StickStateX(0, true));
	ImGui::Text("LX STICK::%f", input::StickStateX(0, false));
	ImGui::Text("RY STICK::%f", input::StickStateY(0, true));
	ImGui::Text("LY STICK::%f", input::StickStateY(0, false));
	ImGui::End();
#endif

	pPlayer.Update(elapsed_time);
	m_camera->Reset(VECTOR3F(Player::GetInstance().GetPositionX() + 30.0f, Player::GetInstance().GetPositionY() + 30.0f, Player::GetInstance().GetPositionZ() + 50.0f),
		VECTOR3F(Player::GetInstance().GetPositionX(), Player::GetInstance().GetPositionY(), Player::GetInstance().GetPositionZ()),
		VECTOR3F(0.0f, 1.0f, 0.0f));
	pStageManager.Updata(elapsed_time);
	m_camera->Update();
}
void SceneGame::Render(ID3D11DeviceContext* immediate_context, float elapsed_time)
{
	if (IsNowLoading())
	{

		return ;
	}
	static VECTOR3F angle(0.0f, 0.0f, 0.0f);
	
	DirectX::XMMATRIX S, R, T;
	S = DirectX::XMMatrixScaling(3.0f, 3.0f, 3.0f);
	R = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
	T = DirectX::XMMatrixTranslation(0.0f, 3.0f, 0.0f);

	DirectX::XMMATRIX W;
	W = S * R * T;

	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1
	);
	

	FLOAT4X4 c, world;

	DirectX::XMStoreFloat4x4(&c, C);
	DirectX::XMStoreFloat4x4(&world, W);

	bs->Activate(immediate_context);
	rs->Activate(immediate_context);
	dss->Activate(immediate_context);
	ss->Activate(immediate_context, 0);

	board->Begin(immediate_context);
	board->Render(immediate_context, m_camera->GetProjection(), m_camera->GetView(), VECTOR3F(0,0,0), 1.0f, VECTOR4F(1, 1, 1, 1));
	board->End(immediate_context);

	pPlayer.Render(immediate_context, c*m_camera->GetView()*m_camera->GetProjection(),VECTOR4F(0,0,-1,0));
	pStageManager.Render(immediate_context, c*m_camera->GetView()*m_camera->GetProjection(), VECTOR4F(0, 0, -1, 0));


	ss->Deactivate(immediate_context);
	dss->Deactivate(immediate_context);
	rs->Deactivate(immediate_context);
	bs->Deactivate(immediate_context);
}