#pragma once
#include "scene.h"

#include <map>
#include <memory>


class SceneManager
{
	static SceneManager* m_instance;

	std::map <std::string, std::unique_ptr<Scene>> m_scenes;
	const char* m_current_scene;

	SceneManager();
	~SceneManager() = default;
public:
	void Execute();
	void ChangeScene(const char* next_scene);


	static void Create()
	{
		if (m_instance != nullptr)
			return;

		m_instance = new SceneManager;
	}
	static SceneManager& GetInstance()
	{
		return *m_instance;
	}
	static void Destroy()
	{
		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}
};

#define ToSTRING(var) #var
enum { TITLE, GAME, };