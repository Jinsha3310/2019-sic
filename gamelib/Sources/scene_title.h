#pragma once
#include "scene.h"

class SceneTitle : public Scene
{
public:
	bool Initialize(ID3D11Device* device, int width, int height);
	void Uninitialize();
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* immediate_context, float elapsed_time);
};