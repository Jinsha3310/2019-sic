#pragma once
#include "scene.h"
#include "camera.h"


#include <memory>
#include <thread>
#include <mutex>

class SceneGame : public Scene
{
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;

	std::unique_ptr<Camera> m_camera;
public:
	~SceneGame() { 
		Uninitialize();
	}

	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	bool Initialize(ID3D11Device* device, int width, int height);
	void Uninitialize();
	void Update(float elapsed_time);
	void Render(ID3D11DeviceContext* immediate_context, float elapsed_time);
};