#pragma once

#include <d3d11.h>

class Scene
{
public:
	Scene() = default;
	virtual ~Scene() = default;

	virtual bool Initialize(ID3D11Device* device, int width, int height) = 0;
	virtual void Uninitialize() = 0;
	virtual void Update(float elapsed_time) = 0;
	virtual void Render(ID3D11DeviceContext* immediate_context, float elapsed_time) = 0;
};