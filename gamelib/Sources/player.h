#pragma once

#include "vector.h"
#include "model.h"
#include "model_renderer.h"

class Player
{
	static Player* m_instance;

	enum ANIMATION_CLIP { CLIP_IDLE, CLIP_RUN, CLIP_NORMAL_ATK1, CLIP_NORMAL_ATK2, CLIP_NORMAL_ATK3, CLIP_SPECIAL_ATK };
	ANIMATION_CLIP	m_animation_clip = CLIP_IDLE;

	enum STATES { STATE_IDLE, STATE_RUN, STATE_NORMAL_ATK, STATE_SPECIAL_ATK };
	STATES m_state = STATE_IDLE;

	VECTOR3F m_position;
	VECTOR3F m_scale;
	VECTOR3F m_rotate;

	int		m_num_combo;
	float	m_combo_reset_timer;
	float	m_attack_power;
	float	m_combo_attack_magnification;

	bool    m_special_attack;

	std::unique_ptr<Model>			m_model;
	std::unique_ptr<ModelRenderer>	m_renderer;

	const float MAX_VELOCITY = 20.0f;
	

	bool IsPlayAnimation() const { return m_model->IsPlayAnimation(); }
	void PlayAnimation(ANIMATION_CLIP animation_index);
	void Animate(const float elapsed_time);


public:

	bool hit;

	const float MAX_COMBO_TIMER = 3.0f;
	const int MAX_COMBO_NUM = 50;

	Player(ID3D11Device* device, std::shared_ptr<ModelResource> resource);
	~Player() = default;
	
	bool Initialize(const VECTOR3F& pos, const VECTOR3F& rotate);
	void Update(const float elapsed_time);
	void Render(ID3D11DeviceContext* immediate_context, const FLOAT4X4& view_projection, const VECTOR4F& light_direction);
	void CountUp() { ++m_num_combo; }

	float GetPositionX() { return m_position.x; }
	float GetPositionY() { return m_position.y; }
	float GetPositionZ() { return m_position.z; }
	float GetRotateY()	 { return m_rotate.y; }

	bool IsAttackMode() { return m_state == STATE_NORMAL_ATK || m_state == STATE_SPECIAL_ATK; }


	void Create(ID3D11Device* device, std::shared_ptr<ModelResource> resource)
	{
		if (m_instance != nullptr)
			return ;

		m_instance = new Player(device, resource);
	}
	static Player& GetInstance()
	{
		return *m_instance;
	}
	bool Exist() { return m_instance != nullptr; }
	void Destory()
	{
		if (m_instance)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}
};

#define pPlayer (Player::GetInstance())