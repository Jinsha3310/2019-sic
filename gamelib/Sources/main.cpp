#include "jinsha_lib.h"
#include "window.h"
#include "scene_manager.h"


int APIENTRY wWinMain(HINSTANCE instance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
	using namespace JinshaLib;
	Initialize(WINDOW_TEXT, SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_MODE, instance);
	SetScreenColor(1, 1, 1);

	SceneManager::Create();
	SceneManager::GetInstance().Execute();
	SceneManager::Destroy();
	
	UnInitialize();
	return 0;
}