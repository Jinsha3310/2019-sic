#include "player.h"
#include "gamepad.h"


Player* Player::m_instance = nullptr;

Player::Player(ID3D11Device* device, std::shared_ptr<ModelResource> resource)
	: m_position(0.0f,0.0f,0.0f), m_scale(0.0f,0.0f,0.0f), m_rotate(0.0f, 0.0f, 0.0f), m_num_combo(0), m_combo_reset_timer(0.0f), m_attack_power(0.0f), m_combo_attack_magnification(0.0f), m_special_attack(false)
{
	m_model = std::make_unique<Model>(resource);
	m_renderer = std::make_unique<ModelRenderer>(device);
}

bool Player::Initialize(const VECTOR3F& pos, const VECTOR3F& rotate)
{
	m_num_combo = 0;
	m_combo_reset_timer = 0.0f;
	m_attack_power = 0.0f;
	m_combo_attack_magnification = 0.0f;

	m_position = pos;
	m_scale = VECTOR3F(0.1f, 0.1f, 0.1f);
	m_rotate = rotate;

	m_special_attack = false;
	return true;
}

void Player::Update(const float elapsed_time)
{
	hit = false;

	static VECTOR3F prev_position;

	VECTOR3F velocity = VECTOR3F(0.0f, 0.0f, 0.0f);

	const float angle_x = input::StickStateX(0, false);
	const float angle_z = input::StickStateY(0, false);
	
	// 0以外なら入力あり
	if (angle_x != 0.0f || angle_z != 0.0f)
	{
		if(m_state != STATES::STATE_NORMAL_ATK &&
			m_state != STATES::STATE_SPECIAL_ATK)
		m_state = STATES::STATE_RUN;
		// 前方ベクトルとプレイヤーベクトルの内積を取り、角度の誤差(sinθ)を出す
		VECTOR3F front_angle_vec(0.0f, 0.0f, -1.0f);
		VECTOR3F player_angle_vec(angle_x, 0.0f, -angle_z);
		player_angle_vec = NormalizeVec3(player_angle_vec);
		float dot_player_front = DotVec3(front_angle_vec, player_angle_vec);
		float sin_sita = acosf(dot_player_front) / 0.01745f;

		// 右ベクトルと内積を取り、左右判定
		VECTOR3F right_angle_vec(1.0f, 0.0f, 0.0f);
		float dot_player_right = DotVec3(right_angle_vec, player_angle_vec);
		if (dot_player_right < 0.0f)
		{
			m_rotate.y = -sin_sita * 0.01745f;
		}
		else if (dot_player_right > 0.0f)
		{
			m_rotate.y = sin_sita * 0.01745f;
		}

		velocity.x += sinf(m_rotate.y) * MAX_VELOCITY;
		velocity.z += cosf(m_rotate.y) * MAX_VELOCITY;
	}
	else
	{
		if (m_state != STATES::STATE_NORMAL_ATK && m_state != STATES::STATE_SPECIAL_ATK)
			m_state = STATES::STATE_IDLE;
	}


	if (input::ButtonRisingState(0, input::PadLabel::B))
	{
		m_state = STATES::STATE_NORMAL_ATK;
		if (m_special_attack)
			m_special_attack = false;
	}
	if (input::ButtonRisingState(0, input::PadLabel::A))
	{
		m_state = STATES::STATE_SPECIAL_ATK;
		if (m_special_attack)
			m_special_attack = false;
	}

	if (m_combo_reset_timer > MAX_COMBO_TIMER)
	{
		m_combo_reset_timer = 0.0f;
		m_num_combo = 0;
	}

	if (m_num_combo > MAX_COMBO_NUM)
	{
		m_special_attack = true;
	}

	prev_position = m_position;
	m_position += velocity*elapsed_time;

	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z);
		R = DirectX::XMMatrixRotationRollPitchYaw(m_rotate.x, m_rotate.y, m_rotate.z);
		T = DirectX::XMMatrixTranslation(m_position.x, m_position.y, m_position.z);

		W = S * R * T;
	}

	m_model->CalculateLocalTransform();
	m_model->CalculateWorldTransform(W);
	Animate(elapsed_time);
}

void Player::Render(ID3D11DeviceContext* immediate_context, const FLOAT4X4& view_projection, const VECTOR4F& light_direction)
{
	m_renderer->Begin(immediate_context, view_projection, light_direction);
	m_renderer->Render(immediate_context, *m_model, VECTOR4F(1.0f, 1.0f, 1.0f, 1.0f));
	m_renderer->End(immediate_context);
}

void Player::PlayAnimation(ANIMATION_CLIP animation_index)
{
	m_animation_clip = animation_index;
	m_model->PlayAnimation(animation_index);
}

void Player::Animate(const float elapsed_time)
{
	static ANIMATION_CLIP prev_animation_clip;
	prev_animation_clip = m_animation_clip;

	switch (m_state)
	{
	case STATES::STATE_IDLE:
		m_animation_clip = CLIP_IDLE;
		break;
	case STATES::STATE_RUN:
		if (m_animation_clip != ANIMATION_CLIP::CLIP_RUN)
			m_animation_clip = ANIMATION_CLIP::CLIP_RUN;
		break;
	case STATES::STATE_NORMAL_ATK:
		if (m_animation_clip != ANIMATION_CLIP::CLIP_NORMAL_ATK1 /*||
			m_animation_clip != ANIMATION_CLIP::CLIP_NORMAL_ATK2*/)
			m_animation_clip = ANIMATION_CLIP::CLIP_NORMAL_ATK1;
		break;
	case STATES::STATE_SPECIAL_ATK:
		if (m_animation_clip != ANIMATION_CLIP::CLIP_NORMAL_ATK2)
			m_animation_clip = ANIMATION_CLIP::CLIP_NORMAL_ATK2;
		break;
	}


	if (m_animation_clip != prev_animation_clip)
	{
		PlayAnimation(m_animation_clip);
	}

	if (!IsPlayAnimation())
	{
		switch (m_animation_clip)
		{
		case ANIMATION_CLIP::CLIP_IDLE:
			m_animation_clip = ANIMATION_CLIP::CLIP_IDLE;
			m_state = STATES::STATE_IDLE;
			break;
		case ANIMATION_CLIP::CLIP_RUN:
			m_animation_clip = ANIMATION_CLIP::CLIP_RUN;
			m_state = STATES::STATE_RUN;
			break;
		case ANIMATION_CLIP::CLIP_NORMAL_ATK1:
			/*if (m_state == STATES::STATE_NORMAL_ATK)
				m_animation_clip = ANIMATION_CLIP::CLIP_NORMAL_ATK2;*/
			m_animation_clip = ANIMATION_CLIP::CLIP_RUN;
			m_state = STATES::STATE_RUN;
			break;
		case ANIMATION_CLIP::CLIP_NORMAL_ATK2:
			//if (m_state == STATES::STATE_NORMAL_ATK)
				m_state = STATES::STATE_RUN;
			break;
		case ANIMATION_CLIP::CLIP_NORMAL_ATK3:
			if (m_state == STATES::STATE_NORMAL_ATK)
				m_animation_clip = ANIMATION_CLIP::CLIP_NORMAL_ATK1;
			break;
		case ANIMATION_CLIP::CLIP_SPECIAL_ATK:
			if (m_state == STATES::STATE_SPECIAL_ATK)
				m_animation_clip = ANIMATION_CLIP::CLIP_RUN;

			break;
		default:
			break;
		}
		PlayAnimation(m_animation_clip);
	}

	m_model->Animate(elapsed_time);
}