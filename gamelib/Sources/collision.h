#pragma once
#include "vector.h"

struct AABB
{
	VECTOR3F left;
	VECTOR3F right;
	VECTOR3F top;
	VECTOR3F bottom;
};

bool IsHitCircle(const VECTOR2F& pos1, const VECTOR2F& pos2, const float radius1, const float radius2);
bool IsHitRect(const VECTOR2F& pos1, const VECTOR4F& size1, const VECTOR2F& pos2, const VECTOR4F& size2);
bool IsHitCapsuleCircle(const VECTOR2F& start, const VECTOR2F& end, const VECTOR2F& c_pos, const float radius1, const float radius2);
//float GetCapsuleCircleDistance(const VECTOR2F& start, const VECTOR2F& end, const VECTOR2F& pos);

bool IsHitBox(const AABB& box1, const VECTOR2F& v1, const AABB& box2, const VECTOR2F& v2);

bool IsHitSphere(const VECTOR3F& pos1, const VECTOR3F& pos2, const float radius1, const float radius2);
bool IsHitCube(const VECTOR3F& min1, const VECTOR3F& max1, const VECTOR3F& min2, const VECTOR3F& max2);
bool IsHitCapsuleSphere(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos, const float radius1, const float radius2);
//float GetCapsuleSphereDistance(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos);