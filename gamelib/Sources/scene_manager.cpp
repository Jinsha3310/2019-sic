#include "scene_manager.h"
#include "scene_title.h"
#include "scene_game.h"
#include "jinsha_lib.h"
#include "window.h"
#include "keyboard.h"
#include "mouse.h"
#include "gamepad.h"


SceneManager* SceneManager::m_instance = nullptr;

using namespace JinshaLib;
SceneManager::SceneManager()
{
	m_scenes[ToSTRING(TITLE)] = std::make_unique<SceneTitle>();
	m_scenes[ToSTRING(GAME)] = std::make_unique<SceneGame>();

	m_current_scene = ToSTRING(GAME);
	m_scenes[m_current_scene]->Initialize(GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT);
}

void SceneManager::Execute()
{
	while (GameLoop())
	{
		//Presents a rendered image to the user
		ClearScreen();


		input::MouseUpdate(JinshaLib::GetHWND());
		input::GamepadUpdate();
		input::KeyboardUpdate();
		m_scenes[m_current_scene]->Update(GetElapsedTime());

		RenderingBegin();
		m_scenes[m_current_scene]->Render(GetContext(), GetElapsedTime());
		RenderingEnd();

		const float average_rendering_time = GetAverageRenderingTime();
		
		//Back screen information to the front screen
		ScreenPresent(1, 0);
	}	
}

void SceneManager::ChangeScene(const char* next_scene)
{
	m_scenes[m_current_scene]->Uninitialize();

	m_current_scene = next_scene;
	m_scenes[m_current_scene]->Initialize(GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT);

	JinshaLib::ResetHighResolutionTimer();
}
