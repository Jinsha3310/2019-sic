#pragma once
#include"breakobj.h"
#include<vector>
struct SaveData
{
	VECTOR3F postion;
	VECTOR3F rotate;
	VECTOR3F min;
	VECTOR3F max;
	int type;
};
class StageManager
{
	std::vector<BreakObj>b_obj;
	std::vector<std::unique_ptr<ModelData>>data;
	std::vector<std::shared_ptr<ModelResource>> model_resource;
	ID3D11Device * de;
#ifdef USE_IMGUI
	std::unique_ptr<ModelData>model_data;
	std::shared_ptr<ModelResource> model_r;
	std::unique_ptr<Model> model;
	std::unique_ptr<Model> hit_area_model;
	VECTOR3F pos;
	VECTOR3F rotate;
	VECTOR3F scale;
	std::unique_ptr<ModelRenderer> renderer;
#endif
public:
	void Initialize(ID3D11Device * device);
	void LoadData();
	void Save();
	void Updata(float elapsed_time);
	void Move_SetPos();
	void Render(ID3D11DeviceContext * immediate_context, const FLOAT4X4&view_projection, const VECTOR4F&light_direction);
	static StageManager&getinctanse()
	{
		static StageManager stagemanager;
		return stagemanager;
	}
};
#define pStageManager (StageManager::getinctanse())