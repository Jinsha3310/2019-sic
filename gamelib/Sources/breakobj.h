#pragma once
#include"model.h"
#include"vector.h"
#include "model_renderer.h"

class BreakObj
{	
	VECTOR3F scale;
	float hp;
	VECTOR3F hit_min;
	VECTOR3F hit_max;
	bool attackhit;
	bool flag;
public:
	VECTOR3F position;
	VECTOR3F rotate;
	int type;
	std::unique_ptr<Model> model;
	BreakObj(){}

	std::unique_ptr<ModelRenderer> renderer;
	BreakObj(BreakObj&&_rt)noexcept
	{
		using std::move;
		position = move(_rt.position);
		scale = move(_rt.scale);
		rotate = move(_rt.rotate);
		model = move(_rt.model);
		renderer = move(_rt.renderer);
		hit_min = move(_rt.hit_min);
		hit_max = move(_rt.hit_max);
		hp = _rt.hp;
		flag = _rt.flag;
		type = _rt.type;
		attackhit = _rt.attackhit;
	}
	auto&operator=(BreakObj&&_rt)noexcept
	{
		if (this != &_rt)
		{
			using std::move;
			position = move(_rt.position);
			scale = move(_rt.scale);
			rotate = move(_rt.rotate);
			model = move(_rt.model);
			renderer = move(_rt.renderer);
			hit_min = move(_rt.hit_min);
			hit_max = move(_rt.hit_max);
			hp = _rt.hp;
			flag = _rt.flag;
			type = _rt.type;
			attackhit = _rt.attackhit;
		}
		return (*this);
	}
	operator const bool() const { return flag; };
	void SetHitArea(const VECTOR3F& area)
	{
		hit_min.x = position.x - area.x;
		hit_min.y = position.y;
		hit_min.z = position.z - area.z;
		hit_max.x = position.x + area.x;
		hit_max.y = position.y + (area.y*2.f);
		hit_max.z = position.z + area.z;

	}
	void Setsize(const VECTOR3F& min, const VECTOR3F& max)
	{
		hit_min = min;
		hit_max = max;
	}
	VECTOR3F&GetMin() { return hit_min; }
	VECTOR3F&GetMax() { return hit_max; }
	void SetAttackHit(const bool&hit)
	{
		attackhit = hit;
	}
	bool&GetAttackHit() { return attackhit; }
	void SetFlag(const bool&f)
	{
		flag = f;
	}
	void Initialize(ID3D11Device * device,std::shared_ptr<ModelResource>model_resource,const int&num, const VECTOR3F&pos, const VECTOR3F&rotation);
	void Updata(float elapsed_time);
	void Render(ID3D11DeviceContext* immediate_context, const FLOAT4X4&view_projection, const VECTOR4F&light_direction);
};
