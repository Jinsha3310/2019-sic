#include "breakobj.h"
void BreakObj::Initialize(ID3D11Device * device, std::shared_ptr<ModelResource>model_resource, const int&num, const VECTOR3F&pos, const VECTOR3F&rotation)
{
	static const float hpset[4] = { 20.f,10.f,30.f,40.f };
	type = num;
	model = std::make_unique<Model>(model_resource);
	scale = VECTOR3F(0.1f, 0.1f, 0.1f);
	rotate = rotation;
	position = pos;
	flag = true;
	hp = hpset[type];
	renderer = std::make_unique<ModelRenderer>(device);
	attackhit = false;
}

void BreakObj::Updata(float elapsed_time)
{
	DirectX::XMMATRIX S, R, T;
	S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	R = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);
	T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);

	DirectX::XMMATRIX W;
	W = S * R * T;
	model->CalculateLocalTransform();
	model->CalculateWorldTransform(W);
}

void BreakObj::Render(ID3D11DeviceContext * immediate_context, const FLOAT4X4&view_projection, const VECTOR4F&light_direction)
{
	
	renderer->Begin(immediate_context, view_projection, light_direction);
	renderer->Render(immediate_context, *model, VECTOR4F(1, 1, 1, 1));
	renderer->End(immediate_context);

}
